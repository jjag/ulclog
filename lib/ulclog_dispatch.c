/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include "ulclog/ulclog_config.h"

#include <stdlib.h>
#include <stdio.h>

#include "ulclog/ulclog_logger_dispatch.h"

static int ulclog_dispatch_vctor(
	struct ulclog_logger *,
	va_list);
static void ulclog_dispatch_dtor(
	struct ulclog_logger *);
static ssize_t ulclog_dispatch_dolog(
	struct ulclog_logger *,
	int,
	const char *,
	ssize_t);

struct LoggerClass DispatchLogger = {
	.size = sizeof(struct ulclog_dispatch_logger),
	.vctor = ulclog_dispatch_vctor,
	.dtor  = ulclog_dispatch_dtor,
	.dolog = ulclog_dispatch_dolog,
};

static int ulclog_dispatch_vinit(
	struct ulclog_dispatch_logger *ulclog_dispatch_logger,
	unsigned int nlogger,
	va_list pa);

int ulclog_dispatch_vinit(
	struct ulclog_dispatch_logger *ulclog_dispatch_logger,
	unsigned int nlogger,
	va_list pa)
{
	unsigned int logger_index;

	ulclog_logger_init(&ulclog_dispatch_logger->ulclog_logger,
			  &DispatchLogger);

	ulclog_dispatch_logger->nlogger = nlogger;
	ulclog_dispatch_logger->subloggers =
		malloc(nlogger * sizeof(ulclog_dispatch_logger->subloggers));

	for (logger_index = 0 ; logger_index < nlogger ; logger_index++) {
		ulclog_dispatch_logger->subloggers[logger_index] =
			va_arg(pa, struct ulclog_logger *);
	}

	return 0;
}

int ulclog_dispatch_init(
	struct ulclog_dispatch_logger *ulclog_dispatch_logger,
	unsigned int nlogger,
	...)
{
	va_list pa;

	va_start(pa, nlogger);

	ulclog_dispatch_vinit(ulclog_dispatch_logger, nlogger, pa);

	va_end(pa);

	return 0;
}

int ulclog_dispatch_vctor(
	struct ulclog_logger *ulclog_logger,
	va_list pa)
{
	unsigned int nlogger;

	nlogger = va_arg(pa, unsigned int);
	ulclog_dispatch_vinit((struct ulclog_dispatch_logger *)ulclog_logger,
			     nlogger, pa);

	return 0;
}

void ulclog_dispatch_clean(
	struct ulclog_dispatch_logger *ulclog_dispatch_logger)
{
	free(ulclog_dispatch_logger->subloggers);
}

void ulclog_dispatch_dtor(
	struct ulclog_logger *ulclog_logger)
{
	ulclog_dispatch_clean((struct ulclog_dispatch_logger *)ulclog_logger);
}

ssize_t ulclog_dispatch_dolog(
	struct ulclog_logger *ulclog_logger,
	int priority,
	const char *message,
	ssize_t nbchar __attribute__((unused)))
{
	unsigned int logger_index;
	ssize_t total_nbchar = 0;
	struct ulclog_dispatch_logger *ulclog_dispatch_logger =
		(struct ulclog_dispatch_logger *)ulclog_logger;

	for (logger_index = 0;
	     logger_index < ulclog_dispatch_logger->nlogger;
	     logger_index++) {
		total_nbchar += ulclog_logger_log_noformat(
			ulclog_dispatch_logger->subloggers[logger_index],
			priority, message);
	}

	return total_nbchar;
}

struct ulclog_logger *ulclog_dispatch_loggger_get_logger(
	struct ulclog_dispatch_logger *ulclog_dispatch_logger,
	unsigned int logger_index)
{
	if (logger_index >= ulclog_dispatch_logger->nlogger)
		return NULL;

	return ulclog_dispatch_logger->subloggers[logger_index];
}
