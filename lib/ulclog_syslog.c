/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include "ulclog/ulclog_config.h"
#include "ulclog/ulclog_logger_syslog.h"

#include <syslog.h>
#include <malloc.h>
#include <stdio.h>


static int ulclog_syslog_vctor(
	struct ulclog_logger *,
	va_list);
static void ulclog_syslog_dtor(
	struct ulclog_logger *);
static ssize_t ulclog_syslog_dolog(
	struct ulclog_logger *,
	int,
	const char *,
	ssize_t);

struct LoggerClass SyslogLogger = {
	.size = sizeof(struct ulclog_logger),
	.vctor = ulclog_syslog_vctor,
	.dtor  = ulclog_syslog_dtor,
	.dolog = ulclog_syslog_dolog,
};

int ulclog_syslog_init(
	struct ulclog_logger *ulclog_logger,
	const char *ident,
	int option,
	int facility)
{
	ulclog_logger_init(ulclog_logger,
			  &SyslogLogger);

	openlog(ident, option, facility);

	return 0;
}

int ulclog_syslog_vctor(
	struct ulclog_logger *ulclog_logger,
	va_list pa)
{
	const char *ident;
	int option;
	int facility;

	ident = va_arg(pa, const char *);
	option = va_arg(pa, int);
	facility = va_arg(pa, int);

	return ulclog_syslog_init(ulclog_logger,
				 ident, option, facility);
}


void ulclog_syslog_clean(
	struct ulclog_logger *ulclog_logger __attribute__((unused)))
{
	closelog();
}
void ulclog_syslog_dtor(
	struct ulclog_logger *ulclog_logger)
{
	ulclog_syslog_clean(ulclog_logger);
}

int ulclog_syslog_setlogmask(
	struct ulclog_logger *logger __attribute__((unused)),
	int mask)
{
	return setlogmask(mask);
}

static ssize_t ulclog_syslog_dolog(
	struct ulclog_logger *logger __attribute__((unused)),
	int priority,
	const char *message,
	ssize_t nbchar)
{
	syslog(priority, "%s", message);

	return nbchar;
}
