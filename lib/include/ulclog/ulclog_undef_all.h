/* Copyright 2016 J�r�me Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

/**
 * \file  ulclog_undef_all.h
 * \brief Undefines everything defined in ulclog.h.
 *
 */


#if defined ULCLOG_H
#undef ULCLOG_H
#undef LOG_PRIORITY_MASK
#undef LOG_PARAMS
#undef PRIO_NAME_0
#undef PRIO_NAME_1
#undef PRIO_NAME_2
#undef PRIO_NAME_3
#undef PRIO_NAME_4
#undef PRIO_NAME_5
#undef PRIO_NAME_6
#undef PRIO_NAME_7
#undef SEP
#undef FILE_LINE_SEP
#undef LOG_MODULE
#undef SHOW_TIME
#undef TIME_DECL
#undef TIME_EVAL
#undef TIME_FORMAT
#undef TIME_VALUE
#undef MODULE_FORMAT
#undef MODULE_VALUE
#undef FUNC_FORMAT
#undef FUNC_VALUE
#undef FILE_LINE_FORMAT
#undef FILE_LINE_VALUE
#undef LEVEL_FORMAT
#undef LEVEL_VALUE
#undef PRE_FORMAT
#undef PRE_VALUE
#undef POST_FORMAT
#undef POST_VALUE
#undef LOG_METHOD
#undef LOG_CONTEXT
#undef GUARD
#undef LOG_PREAMBLE
#undef LOG_AFTERWORD
#undef LOG
#undef EMERGENCY
#undef ALERT
#undef CRITICAL
#undef ERROR
#undef WARNING
#undef NOTICE
#undef INFO
#undef DEB
#endif	/* defined ULCLOG_H */

#if defined ULCLOG_SYSLOG_H
#undef ULCLOG_SYSLOG_H
#endif	/* defined ULCLOG_SYSLOG_H */
