/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

/**
 * \file  ulclog_logger_dispatch.h
 * \brief Defines dispatch logger capabilities.
 *
 */
#ifndef ULCLOG_LOGGER_DISPATCH_H
#define ULCLOG_LOGGER_DISPATCH_H

#include "ulclog/ulclog_logger.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \struct ulclog_dispatch_logger
 * logger wich repeats logs to other loggers
 */
struct ulclog_dispatch_logger {
	struct ulclog_logger ulclog_logger; /**< kind of inheritance */
	unsigned int nlogger;		    /**< number of sub loggers */
	struct ulclog_logger **subloggers;  /**< list of sub loggers */
};

/**
 * \var DispatchLogger
 * The dispatch logger class to be use in ulclog_logger_new.
 * A dispatch logger allow to repeat log to every provided sub
 * loggers.
 */
extern struct LoggerClass DispatchLogger;

/**
 * \fn int ulclog_dispatch_init(
 *             struct ulclog_dispatch_logger *ulclog_dispatch_logger,
 *             unsigned int nlogger, ...)
 * \brief Function to initialize the dispatch logger.
 *
 * This function initializes all internal fields of the dispatch
 * logger. The array of sub loggers is allocated dynamically (malloc).
 *
 * \param ulclog_dispatch_logger to initialize.
 * \param nlogger number of sub loggers..
 * \param ... the nlogger sub loggers.
 *
 * \return 0 (can't fail).
 */
int ulclog_dispatch_init(
	struct ulclog_dispatch_logger *ulclog_dispatch_logger,
	unsigned int nlogger, ...);

/**
 * \fn void ulclog_dispatch_clean(
 *             struct ulclog_dispatch_logger *ulclog_dispatch_logger)
 * \brief Function to clean the dispatch logger.
 *
 * This function cleans internal fields of the dispatch logger. The
 * array of sub loggers is deallocated dynamically (free).
 *
 * \param ulclog_dispatch_logger to clean.
 */
void ulclog_dispatch_clean(
	struct ulclog_dispatch_logger *ulclog_dispatch_logger);

/**
 * \fn struct ulclog_logger *ulclog_dispatch_loggger_get_logger(
 *             struct ulclog_dispatch_logger *ulclog_dispatch_logger,
 *             unsigned int logger_index);
 * \brief Function to get a sub loggerx.
 *
 * This function allow to get sub logger provided at creation.
 *
 * \param ulclog_dispatch_logger to request.
 * \param logger_index of the sub logger to get.
 * \return the concerned sub logger. NULL if the index is invalid.
 */
struct ulclog_logger *ulclog_dispatch_loggger_get_logger(
	struct ulclog_dispatch_logger *ulclog_dispatch_logger,
	unsigned int logger_index);

#ifdef __cplusplus
}
#endif

#endif /* ULCLOG_LOGGER_DISPATCH_H */
