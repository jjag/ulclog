/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

/**
 * \file  ulclog_logger_udp.h
 * \brief Defines syslog logger capabilities.
 *
 */
#ifndef ULCLOG_LOGGER_UDP_H
#define ULCLOG_LOGGER_UDP_H

#include "ulclog/ulclog_logger.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \var UdpLogger
 * The udp logger class to be use in ulclog_logger_new.
 * A udp logger allow uses udp messages as back-end.
 */
extern struct LoggerClass UdpLogger;

/**
 * \struct ulclog_udp_logger
 * logger with udp back-end
 */
struct ulclog_udp_logger {
	struct ulclog_logger ulclog_logger;	  /**< kind of inheritance */
	int s;				  /**< the udp socket  */
	struct sockaddr_in dest_addr_in;  /**< the destination address  */
};

/**
 * \fn int ulclog_udp_init(struct ulclog_udp_logger *ulclog_udp_logger,
 *                         const char *dest_ip,
 *                         unsigned short dest_port)
 * \brief Function to initialize the udp logger.
 *
 * This function initializes the udp logger.
 *
 * \param ulclog_udp_logger to initialize.
 * \param dest_ip is the IPv4 numbers-and-dots notation.
 * \param dest_port is destination udp port.
 *
 * \return 0 if the address/port is valid
 * \return -1 in case of failure.
 */
int ulclog_udp_init(
	struct ulclog_udp_logger *ulclog_udp_logger,
	const char *dest_ip,
	unsigned short dest_port);

/**
 * \fn void ulclog_udp_clean(struct ulclog_udp_logger *ulclog_udp_logger)
 * \brief Function to clean the udp logger.
 *
 * This function cleans udp back-end.
 *
 * \param ulclog_udp_logger to clean.
 */
void ulclog_udp_clean(
	struct ulclog_udp_logger *ulclog_udp_logger);

/**
 * \fn int ulclog_udp_setsockopt(struct ulclog_udp_logger *ulclog_udp_logger,
 *                               int level,
 *                               int optname,
 *                               const void *optval,
 *                               socklen_t optlen)
 * \brief Function to set udp socket options(See setsockopt(2)).
 *
 * This function sets udp socket options(Call to setsockopt(2)).
 *
 * \param ulclog_udp_logger to modify.
 * \param level as in setsockopt(2).
 * \param optname as in setsockopt(2).
 * \param optval as in setsockopt(2).
 * \param optlen as in setsockopt(2).
 *
 * \return 0 on success.
 * \return -1 on error and errno is set appropriately.
 */
int ulclog_udp_setsockopt(
	struct ulclog_udp_logger *ulclog_udp_logger,
	int level,
	int optname,
	const void *optval,
	socklen_t optlen);

/**
 * \fn int ulclog_udp_getsockopt(struct ulclog_udp_logger *ulclog_udp_logger,
 *                               int level,
 *                               int optname,
 *                               void *optval,
 *                               socklen_t *optlen)
 * \brief Function to get udp socket options(See getsockopt(2)).
 *
 * This function gets udp socket options(Call to getsockopt(2)).
 *
 * \param ulclog_udp_logger to modify.
 * \param level as in getsockopt(2).
 * \param optname as in getsockopt(2).
 * \param optval as in getsockopt(2).
 * \param optlen as in getsockopt(2).
 *
 * \return 0 on success.
 * \return -1 on error and errno is set appropriately.
 */
int ulclog_udp_getsockopt(
	struct ulclog_udp_logger *ulclog_udp_logger,
	int level,
	int optname,
	void *optval,
	socklen_t *optlen);

#ifdef __cplusplus
}
#endif

#endif /* ULCLOG_LOGGER_UDP_H */
