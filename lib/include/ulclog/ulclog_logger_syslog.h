/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

/**
 * \file  ulclog_logger_syslog.h
 * \brief Defines syslog logger capabilities.
 *
 */
#ifndef ULCLOG_LOGGER_SYSLOG_H
#define ULCLOG_LOGGER_SYSLOG_H

#include "ulclog/ulclog_logger.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \var SyslogLogger
 * The syslog logger class to be use in ulclog_logger_new.
 * A syslog logger allow uses syslog as back-end.
 */
extern struct LoggerClass SyslogLogger;

/**
 * \fn int ulclog_syslog_init(struct ulclog_logger *ulclog_logger,
 *                            const char *ident,
 *                            int option,
 *                            int facility)
 * \brief Function to initialize the syslog logger.
 *
 * This function initializes the syslog back-end (call to openlog(3)).
 *
 * \param ulclog_logger to initialize.
 * \param ident as in openlog(3).
 * \param option as in openlog(3).
 * \param facility as in openlog(3).
 *
 * \return 0 (can't fail).
 */
int ulclog_syslog_init(
	struct ulclog_logger *ulclog_logger,
	const char *ident,
	int option,
	int facility);

/**
 * \fn void ulclog_syslog_clean(
 *             struct ulclog_logger *ulclog_logger)
 * \brief Function to clean the syslog logger.
 *
 * This function cleans syslog back-end. Call to closelog(3)
 *
 * \param ulclog_logger to clean.
 */
void ulclog_syslog_clean(
	struct ulclog_logger *ulclog_logger);

/**
 * \fn int ulclog_syslog_setlogmask(struct ulclog_logger *ulclog_logger,
 *                                  int mask)
 * \brief Function to set the priority mask at syslog level.
 *
 * This function sets the priority mask at syslog level. Call to
 * setlogmask(3)
 *
 * \param ulclog_logger to modify.
 * \param mask as in setlogmask(3).
 *
 * \return the previous log priority mask.
 */
int ulclog_syslog_setlogmask(
	struct ulclog_logger *ulclog_logger,
	int mask);

#ifdef __cplusplus
}
#endif

#endif /* ULCLOG_LOGGER_SYSLOG_H */
