/* Copyright 2016 J�r�me Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

/**
 * \file  ulclog.h
 * \brief Defines logging capabilities - everything can be redefined.
 *
 */

#ifndef ULCLOG_H
#define ULCLOG_H

#include <unistd.h>
#include <syslog.h>
#include <stdio.h>

/*!
  \def LOG_PARAMS
  Helper to built list of params
*/
#define LOG_PARAMS(...)			__VA_ARGS__

#if !defined LOG_PRIORITY_MASK
/*!
  \def LOG_PRIORITY_MASK
  Default log priority mask (As defined in sys/syslog.h).
*/
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_INFO)
#endif	/* !defined LOG_PRIORITY_MASK */

#if !defined PRIO_NAME_0
/*!
  \def PRIO_NAME_0
  Priority names for level 0
*/
#define PRIO_NAME_0		"emerg"
/*!
  \def PRIO_NAME_1
  Priority names for level 1
*/
#define PRIO_NAME_1		"alert"
/*!
  \def PRIO_NAME_2
  Priority names for level 2
*/
#define PRIO_NAME_2		"crit"
/*!
  \def PRIO_NAME_3
  Priority names for level 3
*/
#define PRIO_NAME_3		"err"
/*!
  \def PRIO_NAME_4
  Priority names for level 4
*/
#define PRIO_NAME_4		"warning"
/*!
  \def PRIO_NAME_5
  Priority names for level 5
*/
#define PRIO_NAME_5		"notice"
/*!
  \def PRIO_NAME_6
  Priority names for level 6
*/
#define PRIO_NAME_6		"info"
/*!
  \def PRIO_NAME_7
  Priority names for level 7
*/
#define PRIO_NAME_7		"debug"
#endif	/* !defined PRIO_NAME_0 */

#if !defined SEP
/*!
  \def SEP
  Fields separator.
*/
#define SEP		     ":"
#endif	/* !defined SEP */

#if !defined FILE_LINE_SEP
/*!
  \def FILE_LINE_SEP
  Separator between File and Line.
*/
#define FILE_LINE_SEP	     ":"
#endif	/* !defined FILE_LINE_SEP */

#if !defined LOG_MODULE
/*!
  \def LOG_MODULE
  Module name. Should probably redefined in each c file.
*/
#define LOG_MODULE __FILE__
	#warning LOG_MODULE should be defined.
#endif	/* !defined MODULE */

#if !defined SHOW_TIME
/*!
  \def SHOW_TIME
  Option to incude or not the time in each log.
*/

#define SHOW_TIME 1
#endif	/* !defined SHOW_TIME */

#if SHOW_TIME

#if HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY
#include <sys/time.h>
#else /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */
#include <time.h>
#endif /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */

#if !defined TIME_DECL
/*!
  \def TIME_DECL
  Allow to declare variables to get the time.
*/
#if HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY
#define TIME_DECL struct timeval t
#else /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */
#define TIME_DECL
#endif /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */
#endif	/* !defined TIME_DECL */

#if !defined TIME_EVAL
/*!
  \def TIME_EVAL
  Method to get the time.
*/
#if HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY
#define TIME_EVAL				\
	do {					\
		gettimeofday(&t, NULL);		\
	} while (0)
#else /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */
#define TIME_EVAL

#endif /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */
#endif /* !defined TIME_EVAL */

#if !defined TIME_FORMAT
/*!
  \def TIME_FORMAT
  Format to show the time.
*/
#if HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY
#define TIME_FORMAT	     "%ld.%06ld"
#else /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */
#define TIME_FORMAT	     "%ld"
#endif /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */
#endif	/* !defined TIME_FORMAT */

#if !defined TIME_VALUE
/*!
  \def TIME_VALUE
  How to retreive the time value.
*/
#if HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY
#define TIME_VALUE	LOG_PARAMS(t.tv_sec, t.tv_usec)
#else /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */
#define TIME_VALUE	time(NULL)
#endif /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */
#endif	/* !defined TIME_VALUE */

#else  /* SHOW_TIME */
#define TIME_DECL
#define TIME_EVAL
#define TIME_FORMAT
#define TIME_VALUE
#endif	/* SHOW_TIME */


#if !defined MODULE_FORMAT
/*!
  \def MODULE_FORMAT
  Format to show the module.
*/
#define MODULE_FORMAT	     "%-10s"
#endif	/* !defined MODULE_FORMAT */

#if !defined MODULE_VALUE
/*!
  \def MODULE_VALUE
  How to retreive the module value.
*/
#define MODULE_VALUE	LOG_PARAMS(LOG_MODULE)
#endif	/* !defined MODULE_VALUE */

#if !defined FUNC_FORMAT
/*!
  \def FUNC_FORMAT
  Format to show the function name.
*/
#define FUNC_FORMAT	     "%-10s"
#endif	/* !defined FUNC_FORMAT */

#if !defined FUNC_VALUE
/*!
  \def FUNC_VALUE
  How to retreive the function name.
*/
#define FUNC_VALUE	      LOG_PARAMS(__func__)
#endif	/* !defined FUNC_VALUE */

#if !defined FILE_LINE_FORMAT
/*!
  \def FILE_LINE_FORMAT
  Format to show the file/line.
*/
#define FILE_LINE_FORMAT	"%-11s" FILE_LINE_SEP "%d"
#endif	/* !defined FILE_LINE_FORMAT */

#if !defined FILE_LINE_VALUE
/*!
  \def FILE_LINE_VALUE
  How to retreive the file/line.
*/
#define FILE_LINE_VALUE	        LOG_PARAMS(__FILE__,  __LINE__)
#endif	/* !defined FILE_LINE_VALUE */

#if !defined LEVEL_FORMAT
/*!
  \def LEVEL_FORMAT
  Format to show the level.
*/
#define LEVEL_FORMAT	     "%-7s" SEP "%1d"
#endif	/* !defined LEVEL_FORMAT */

#if !defined LEVEL_VALUE
/*!
  \def LEVEL_VALUE
  How to retreive the level.
*/
#define LEVEL_VALUE(level)			\
	LOG_PARAMS(PRIO_NAME_ ##level, level)
#endif	/* !defined LEVEL_VALUE */

#if !defined PRE_FORMAT
/*!
  \def PRE_FORMAT
  Format of data before user log.
*/
#if SHOW_TIME
#define PRE_FORMAT				\
	TIME_FORMAT SEP				\
	LEVEL_FORMAT SEP			\
	MODULE_FORMAT SEP			\
	FUNC_FORMAT SEP ""
#else  /* SHOW_TIME */
#define PRE_FORMAT				\
	LEVEL_FORMAT SEP			\
	MODULE_FORMAT SEP			\
	FUNC_FORMAT SEP ""
#endif /* SHOW_TIME */
#endif /* !defined PRE_FORMAT */

#if !defined PRE_VALUE
/*!
  \def PRE_VALUE
  How to retreive the data before user log.
*/
#if SHOW_TIME
#define PRE_VALUE(level)						\
	LOG_PARAMS(TIME_VALUE, LEVEL_VALUE(level), MODULE_VALUE, FUNC_VALUE)
#else  /* SHOW_TIME */
#define PRE_VALUE(level)						\
	LOG_PARAMS(LEVEL_VALUE(level), MODULE_VALUE, FUNC_VALUE)
#endif	/* SHOW_TIME */
#endif /* !defined PRE_VALUE */

#if !defined POST_FORMAT
/*!
  \def POST_FORMAT
  Format of data after user log.
*/
#define POST_FORMAT	     "\n"
#endif /* !defined POST_FORMAT */

#if !defined POST_VALUE
/*!
  \def POST_VALUE
  How to retreive the data after user log.
*/
#define POST_VALUE
/* Need to be defined for doxygen... undef for code below */
#undef POST_VALUE
#endif	/* !defined POST_VALUE */

#if !defined LOG_METHOD
/*!
  \def LOG_METHOD
  The method tp log.
*/
#define LOG_METHOD	     fprintf
#endif	/* !defined LOG_METHOD */

#if !defined LOG_CONTEXT
/*!
  \def LOG_CONTEXT
  The first parameter to be used for LOG_METHOD.
*/
# define LOG_CONTEXT(level)  stderr
#endif	/* !defined LOG_CONTEXT */

#if !defined GUARD
/*!
  \def GUARD
  Run time quard.
*/
#define GUARD (1)
#endif	/* !defined GUARD */

#if !defined LOG_PREAMBLE
/*!
  \def LOG_PREAMBLE
  ALlow to execute some code before doing the log.
*/
#define LOG_PREAMBLE(level)
#endif	/* !defined LOG_PREAMBLE */

#if !defined LOG_AFTERWORD
/*!
  \def LOG_AFTERWORD
  ALlow to execute some code afetr doing the log.
*/
#define LOG_AFTERWORD(level)
#endif	/* !defined LOG_AFTERWORD */

#if !defined LOG
/*!
  \def LOG(level, format, ...)
  Do a log at the given \a level, with the given \a format and the
  provided \a args.
*/
#if defined POST_VALUE
#define LOG(level, format, ...)					\
	({							\
		ssize_t _nbchar = 0;				\
		if ((LOG_PRIORITY_MASK & LOG_MASK(level))	\
		    &&						\
		    GUARD) {					\
			TIME_DECL;				\
			LOG_PREAMBLE(level);			\
			TIME_EVAL;				\
			_nbchar = (signed)LOG_METHOD(		\
				LOG_CONTEXT(level),		\
				PRE_FORMAT format POST_FORMAT,	\
				LOG_PARAMS(PRE_VALUE(level),	\
					   ##__VA_ARGS__,	\
					   POST_VALUE));	\
			LOG_AFTERWORD(level);			\
		}						\
		_nbchar;					\
	})
#else  /* defined POST_VALUE */
#define LOG(level, format, ...)					\
	({							\
		ssize_t _nbchar = 0;				\
		if ((LOG_PRIORITY_MASK & LOG_MASK(level))	\
		    &&						\
		    GUARD) {					\
			TIME_DECL;				\
			LOG_PREAMBLE(level);			\
			TIME_EVAL;				\
			_nbchar = (signed)LOG_METHOD(		\
				LOG_CONTEXT(level),		\
				PRE_FORMAT format POST_FORMAT,	\
				LOG_PARAMS(PRE_VALUE(level),	\
					   ##__VA_ARGS__));	\
			LOG_AFTERWORD(level);			\
		}						\
		_nbchar;					\
	})
#endif  /* defined POST_VALUE */
#endif /* !defined LOG */

#if !defined LEMERGENCY
/*!
  \def LEMERGENCY(format, ...)
  Do a log at the level LOG_EMERG, with the given \a format and the
  provided \a args.
*/
#define LEMERGENCY(format, ...)			\
	LOG(LOG_EMERG, format, ##__VA_ARGS__)
#endif /* !defined LEMERGENCY */

#if !defined LALERT
/*!
  \def LALERT(format, ...)
  Do a log at the level LOG_ALERT, with the given \a format and the
  provided \a args.
*/
#define LALERT(format, ...)			\
	LOG(LOG_ALERT, format, ##__VA_ARGS__)
#endif	/* !defined LALERT */

#if !defined LCRITICAL
/*!
  \def LCRITICAL(format, ...)
  Do a log at the level LOG_CRIT, with the given \a format and the
  provided \a args.
*/
#define LCRITICAL(format, ...)			\
	LOG(LOG_CRIT, format, ##__VA_ARGS__)
#endif	/* !defined LCRITICAL */

#if !defined LERROR
/*!
  \def LERROR(format, ...)
  Do a log at the level LOG_ERR, with the given \a format and the
  provided \a args.
*/
#define LERROR(format, ...)			\
	LOG(LOG_ERR, format, ##__VA_ARGS__)
#endif	/* !defined LERROR */

#if !defined LWARNING
/*!
  \def LWARNING(format, ...)
  Do a log at the level LOG_WARNING, with the given \a format and the
  provided \a args.
*/
#define LWARNING(format, ...)			\
	LOG(LOG_WARNING, format, ##__VA_ARGS__)
#endif	/* !defined LWARNING */

#if !defined LNOTICE
/*!
  \def LNOTICE(format, ...)
  Do a log at the level LOG_NOTICE, with the given \a format and the
  provided \a args.
*/
#define LNOTICE(format, ...)			\
	LOG(LOG_NOTICE, format, ##__VA_ARGS__)
#endif	/* !defined LNOTICE */

#if !defined LINFO
/*!
  \def LINFO(format, ...)
  Do a log at the level LOG_INFO, with the given \a format and the
  provided \a args.
*/
#define LINFO(format, ...)			\
	LOG(LOG_INFO, format, ##__VA_ARGS__)
#endif	/* !defined LINFO */

#if !defined LDEBUG
/*!
  \def LDEBUG(format, ...)
  Do a log at the level LOG_DEBUG, with the given \a format and the
  provided \a args.
*/
#define LDEBUG(format, ...)				\
	LOG(LOG_DEBUG, FILE_LINE_FORMAT SEP format,	\
	    LOG_PARAMS(FILE_LINE_VALUE, ##__VA_ARGS__))
#endif /* !defined LDEBUG */

#endif /* ULCLOG_H */
