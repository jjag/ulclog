/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

/**
 * \file  ulclog_logger.h
 * \brief Defines ulclog logger capabilities.
 *
 */
#ifndef ULCLOG_LOGGER_H
#define ULCLOG_LOGGER_H

#include <stdarg.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \struct ulclog_logger
 * generic logger
 */
struct ulclog_logger {
	const struct LoggerClass *class; /**< the class of the
					  * logger */
	int _mask;			 /**< the internal priority
					  * mask */
	int *pmask;			 /**< pointer on the mask
					  * (internal or external) */
	/** the current decorator */
	struct ulclog_logger_decorator *decorator;
};

/**
 * \struct LoggerClass
 *
 * Structure to describe the a logger class:
 *   - Allow to allocate a logger thanks to it's size.
 *   - store the methods.
 */
struct LoggerClass {
	/** size of the structure to allocate */
	size_t size;
	/** constructor method (variadic) */
	int (*vctor)(struct ulclog_logger *, va_list);
	/** destructor method */
	void (*dtor)(struct ulclog_logger *);
	/** log method */
	ssize_t (*dolog)(struct ulclog_logger *, int priority,
			 const char *, ssize_t nbchar);
};

/**
 * \fn struct ulclog_logger *ulclog_logger_new(
 *             const struct LoggerClass *loggerClass, ...);
 * \brief Function to create logger object thanks to provide logger class.
 *
 * This function calls the contructors of the object.
 *
 * \param loggerClass to use to create the new logger.
 * \param ... Variadic parameters depending on each logger.
 *
 * \return a pointer on the created and constructed logger or NULL (if
 * allocation failed).
 */
struct ulclog_logger *ulclog_logger_new(
	const struct LoggerClass *loggerClass,
	...);

/**
 * \fn void ulclog_logger_delete(struct ulclog_logger *ulclog_logger)
 * \brief Function to delete the logger.
 *
 * This function calls the destructors of the class.
 *
 * \param ulclog_logger to delete.
 */
void ulclog_logger_delete(
	struct ulclog_logger *ulclog_logger);

/**
 * \fn int ulclog_logger_init(struct ulclog_logger *ulclog_logger,
 *                            const struct LoggerClass *loggerClass);
 * \brief Function to initialize the common part of all logger classes
 *
 * This function initializes all fields of struct ulclog_logger.
 *
 * \param ulclog_logger to initialize.
 * \param loggerClass to set.
 * \return 0 (can't fail)
 */
int ulclog_logger_init(
	struct ulclog_logger *ulclog_logger,
	const struct LoggerClass *loggerClass);

/**
 * \fn ssize_t ulclog_logger_log(struct ulclog_logger *ulclog_logger,
 *                               int priority,
 *                               const char *format, ...)
 * \brief Function to log
 *
 * This function logs a message using the concrete logger if the
 * priority is set in the logger's priority mask.
 *
 * \param ulclog_logger to use to log.
 * \param priority to use to log (see syslog(3)).
 * \param format of the string to log.
 * \param ... the arguments depending on the provided format.
 * \return the number of logged characters.
 */
ssize_t ulclog_logger_log(
	struct ulclog_logger *ulclog_logger,
	int priority,
	const char *format, ...) __attribute__((format(printf, 3, 4)));

/**
 * \fn ssize_t ulclog_logger_log_noformat(struct ulclog_logger *ulclog_logger,
 *                                        int priority,
 *                                        const char *message)
 * \brief Function to log without format
 *
 * This function logs a message using the concrete logger if the
 * priority is set in the logger's priority mask.
 *
 * \param ulclog_logger to use to log.
 * \param priority to use to log (see syslog(3)).
 * \param message to log.
 * \return the number of logged characters.
 */
ssize_t ulclog_logger_log_noformat(
	struct ulclog_logger *ulclog_logger,
	int priority,
	const char *message);

/**
 * \fn int ulclog_logger_set_prio_mask(struct ulclog_logger *ulclog_logger,
 *                                     int new_mask)
 * \brief Function to change the priority mask.
 *
 * This function modifies the priority mask to modify
 * ulclog_logger_log behavior.
 *
 * \param ulclog_logger to modify.
 * \param new_mask to use
 * \return the previous used mask.
 */
int ulclog_logger_set_prio_mask(
	struct ulclog_logger *ulclog_logger,
	int new_mask);

/**
 * \fn void ulclog_logger_set_external_mask(struct ulclog_logger *ulclog_logger,
 *                                          int *external_mask);
 * \brief Function to provide an external mask address.
 *
 * By default, the priority mask is stored in the logger (mask
 * field). This can be changed to provide an external mask (from a
 * shared memory for example). This can be usefull to modify the
 * priority mask without function call.
 *
 * \param ulclog_logger to modify.
 * \param external_mask address to use. If NULL, come back to default behavior
 */
void ulclog_logger_set_external_mask(
	struct ulclog_logger *ulclog_logger,
	int *external_mask);


/**
 * \struct ulclog_logger_decorator
 * Logger decorator
 */
struct ulclog_logger_decorator {
	/** Allow user to store decorator variables */
	void *context;
	/** Will be executed before user message building */
	ssize_t (*before)(void *context,
			  struct ulclog_logger *ulclog_logger,
			  int priority,
			  char *message,
			  ssize_t message_buffer_size,
			  ssize_t offset);
	/** Will be executed after user message building */
	ssize_t (*after)(void *context,
			 struct ulclog_logger *ulclog_logger,
			 int priority,
			 char *message,
			 ssize_t message_buffer_size,
			 ssize_t offset);
};


/**
 * \fn void ulclog_logger_set_decorator(
 *             struct ulclog_logger *ulclog_logger,
 *             struct ulclog_logger_decorator *decorator)
 * \brief Function to set a decorator.
 *
 * A decorator allow to modify user message before and after having
 * build the message himself.
 *
 * \param ulclog_logger to modify.
 * \param decorator to install. If NULL, remove the decorator
 */
void ulclog_logger_set_decorator(
	struct ulclog_logger *ulclog_logger,
	struct ulclog_logger_decorator *decorator);

#ifdef __cplusplus
}
#endif

#endif /* ULCLOG_LOGGER_H */
