/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

/**
 * \file  ulclog_logger_septhread.h
 * \brief Defines separate thread logger capabilities.
 *
 */
#ifndef ULCLOG_LOGGER_SEPTHREAD_H
#define ULCLOG_LOGGER_SEPTHREAD_H

#include <mqueue.h>
#include <pthread.h>
#include <stdatomic.h>

#include "ulclog/ulclog_logger.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \var SepThreadLogger
 *
 * The separate thread logger class to be use in ulclog_logger_new. A
 * Separate Thread logger allow to delegate in a separate thread the
 * real logging. It allow to limit the performanc impact on the
 * functionnal thread.
 */
extern struct LoggerClass SepThreadLogger;

/** \brief Callback called when some messages are missed.
 *
 * \param ulclog_logger the sublogger of the septhread logger.
 * \param nmissed number of missed regular log messages.
 *
 * Callback that give the opportunity to log error message when some
 * messages are missed.
 */
typedef void (*missed_msg_cb_t)(
	struct ulclog_logger *ulclog_logger,
	unsigned int nmissed);
/**
 * \struct ulclog_septhread_logger
 * logger with separate thread back-end
 */
struct ulclog_septhread_logger {
	struct ulclog_logger ulclog_logger;  /**< kind of inheritance */
	struct ulclog_logger *sublogger;     /**< the sublogger used
					      * in the thread */
	pthread_t thread;	             /**< the logger thread  */
	mqd_t mqdes;		             /**< the queue to send
					      * the messages */
	atomic_uint seq_number;	             /**< to detect missed messages */
	missed_msg_cb_t missed_msg_cb;       /**< to notify user when
					      * some messages are missed */
};

/**
 * \fn int ulclog_septhread_init(
 *             struct ulclog_septhread_logger *ulclog_septhread_logger_logger,
 *             struct ulclog_logger *sublogger,
 *             const pthread_attr_t *attr,
 *             long mq_maxmsg,
 *             missed_msg_cb_t missed_msg_cb)
 * \brief Function to initialize the separate thread logger.
 *
 * This function initializes the separate thread logger.
 *
 * \param ulclog_septhread_logger_logger to initialize.
 * \param sublogger is the logger witch will be used by the separate
 * thread logger.
 * \param attr is used for thread creattion. If NULL, default values
 * will be used. If provided, the detachstate will be forces to
 * JOINABLE.
 * \param mq_maxmsg is the max number of messages stored in the
 * message queue used to exchange data with the separate thread.
 * \param missed_msg_cb to notify user when some messages are missed
 *
 * \return 0 if everything is OK.
 * \return -1 in case of failure.
 */
int ulclog_septhread_init(
	struct ulclog_septhread_logger *ulclog_septhread_logger,
	struct ulclog_logger *sublogger,
	const pthread_attr_t *attr,
	long mq_maxmsg,
	missed_msg_cb_t missed_msg_cb);

/**
 * \fn void ulclog_septhread_clean(
 *             struct ulclog_septhread_logger *ulclog_septhread_logger)
 * \brief Function to clean the septhread logger.
 *
 * This function cleans septhread back-end.
 *
 * \param ulclog_septhread_logger to clean.
 */
void ulclog_septhread_clean(
	struct ulclog_septhread_logger *ulclog_septhread_logger);

#ifdef __cplusplus
}
#endif

#endif /* ULCLOG_LOGGER_SEPTHREAD_H */
