/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include "ulclog/ulclog_config.h"

#include "ulclog/ulclog_logger.h"
#include <malloc.h>
#include <stdarg.h>
#include <string.h>

#define LOG_MODULE "ulclog_logger"
#include "ulclog/ulclog.h"

struct ulclog_logger *ulclog_logger_new(
	const struct LoggerClass *loggerClass,
	...)
{
	struct ulclog_logger *ulclog_logger;
	va_list pa;
	int ctor_return_value;

	ulclog_logger = malloc(loggerClass->size);

	ulclog_logger_init(ulclog_logger, loggerClass);

	va_start(pa, loggerClass);

	ctor_return_value = loggerClass->vctor(ulclog_logger, pa);

	va_end(pa);

	if (ctor_return_value < 0) {
		free(ulclog_logger);
		ulclog_logger = NULL;
	}

	return ulclog_logger;
}

int ulclog_logger_init(
	struct ulclog_logger *ulclog_logger,
	const struct LoggerClass *loggerClass)
{
	ulclog_logger->class = loggerClass;
	ulclog_logger->_mask = LOG_UPTO(LOG_INFO);
	ulclog_logger->pmask = &ulclog_logger->_mask;
	ulclog_logger->decorator = NULL;

	return 0;
}

void ulclog_logger_delete(
	struct ulclog_logger *logger)
{
	logger->class->dtor(logger);
	free(logger);
}

ssize_t ulclog_logger_log(
	struct ulclog_logger *ulclog_logger,
	int priority,
	const char *format,
	...)
{
	char message[LOG_BUFFERS_SIZE];
	va_list ap;

	va_start(ap, format);

	vsnprintf(message, sizeof(message), format, ap);

	va_end(ap);

	return ulclog_logger_log_noformat(ulclog_logger,
					 priority,
					 message);
}

ssize_t ulclog_logger_log_noformat(
	struct ulclog_logger *ulclog_logger,
	int priority,
	const char *message)
{
	ssize_t nbchar = 0;
	char new_message[LOG_BUFFERS_SIZE];

	LDEBUG("*ulclog_logger->pmask [%02x] LOG_MASK(priority) [%02x]",
	       (unsigned int)*ulclog_logger->pmask,
	       (unsigned int)LOG_MASK(priority));

	if ((*ulclog_logger->pmask & LOG_MASK(priority)) == 0)
		return 0;

	if (!ulclog_logger->decorator) {
		nbchar = (signed)strlen(message);
	} else {
		if (ulclog_logger->decorator->before)
			nbchar = ulclog_logger->decorator->before(
				ulclog_logger->decorator->context,
				ulclog_logger,
				priority,
				new_message,
				(signed)sizeof(new_message),
				0);

		strncpy(new_message + nbchar, message,
			sizeof(new_message) - (unsigned)nbchar);
		nbchar +=  (signed)strlen(message);

		if (ulclog_logger->decorator->after)
			nbchar += ulclog_logger->decorator->after(
				ulclog_logger->decorator->context,
				ulclog_logger,
				priority,
				new_message,
				(signed)sizeof(new_message),
				nbchar);
		message = new_message;
	}

	nbchar = ulclog_logger->class->dolog(ulclog_logger, priority,
					    message, nbchar);

	return nbchar;
}

int ulclog_logger_set_prio_mask(
	struct ulclog_logger *ulclog_logger,
	int mask)
{
	int previous_prio_mask = *ulclog_logger->pmask;

	*ulclog_logger->pmask = mask;

	return previous_prio_mask;
}

void ulclog_logger_set_external_mask(
	struct ulclog_logger *ulclog_logger,
	int *external_mask)
{
	if (external_mask == NULL) {
		ulclog_logger->_mask = *ulclog_logger->pmask;
		ulclog_logger->pmask = &ulclog_logger->_mask;
	} else {
		ulclog_logger->pmask = external_mask;
	}
}

void ulclog_logger_set_decorator(
	struct ulclog_logger *ulclog_logger,
	struct ulclog_logger_decorator *decorator)
{
	ulclog_logger->decorator = decorator;
}
