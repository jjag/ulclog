/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include "ulclog/ulclog_config.h"

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <errno.h>
#include <string.h>

#include "ulclog/ulclog_logger_udp.h"

#define LOG_MODULE "ulclog_udp"
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_WARNING)
#include "ulclog/ulclog.h"

static int ulclog_udp_vctor(
	struct ulclog_logger *, va_list);
static void ulclog_udp_dtor(
	struct ulclog_logger *);
static ssize_t ulclog_udp_dolog(
	struct ulclog_logger *,
	int,
	const char *,
	ssize_t);

struct LoggerClass UdpLogger = {
	.size = sizeof(struct ulclog_udp_logger),
	.vctor = ulclog_udp_vctor,
	.dtor  = ulclog_udp_dtor,
	.dolog = ulclog_udp_dolog,
};

int ulclog_udp_init(
	struct ulclog_udp_logger *ulclog_udp_logger,
	const char *dest_ip,
	unsigned short dest_port)
{
	ulclog_logger_init(&ulclog_udp_logger->ulclog_logger,
			  &UdpLogger);

	if (dest_port == 0) {
		LERROR("Bad destination port: %hu", dest_port);
		return -1;
	}

	if (inet_pton(AF_INET, dest_ip,
		      &ulclog_udp_logger->dest_addr_in.sin_addr) == 0) {
		LERROR("Bad destination IP: %s", dest_ip);
		return -1;
	}

	ulclog_udp_logger->dest_addr_in.sin_family = AF_INET;
	ulclog_udp_logger->dest_addr_in.sin_port = htons(dest_port);

	ulclog_udp_logger->s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	return 0;
}

int ulclog_udp_vctor(
	struct ulclog_logger *ulclog_logger,
	va_list pa)
{
	const char *dest_ip;
	int dest_port_int;
	unsigned short dest_port;

	dest_ip = va_arg(pa, const char *);
	dest_port_int = va_arg(pa, int);

	LDEBUG("dest_ip [%s] dest_port_int [%d] USHRT_MAX [%hu]",
	       dest_ip, dest_port_int, USHRT_MAX);

	if ((dest_port_int > (int)USHRT_MAX) || (dest_port_int < 0)) {
		LERROR("Bad destination port: %d", dest_port_int);
		return -1;
	}

	dest_port = (unsigned short int)dest_port_int;
	return ulclog_udp_init((struct ulclog_udp_logger *)ulclog_logger,
			      dest_ip, dest_port);
}


void ulclog_udp_clean(
	struct ulclog_udp_logger *ulclog_udp_logger)
{
	close(ulclog_udp_logger->s);
}

void ulclog_udp_dtor(
	struct ulclog_logger *ulclog_logger)
{
	ulclog_udp_clean((struct ulclog_udp_logger *)ulclog_logger);
}

static ssize_t ulclog_udp_dolog(
	struct ulclog_logger *ulclog_logger,
	int priority  __attribute__((unused)),
	const char *message,
	ssize_t nbchar)
{
	struct ulclog_udp_logger *ulclog_udp_logger =
		(struct ulclog_udp_logger *)ulclog_logger;
	ssize_t message_length;

	message_length = sendto(
		ulclog_udp_logger->s, message,
		(unsigned)nbchar + 1, 0,
		(struct sockaddr *)&ulclog_udp_logger->dest_addr_in,
		(int)sizeof(ulclog_udp_logger->dest_addr_in));

	if (message_length != nbchar + 1) {
		LERROR("sendto failed: errno [%d] strerror [%s]",
		       errno, strerror(errno));
		return message_length;
	} else {
		return nbchar;
	}
}

int ulclog_udp_setsockopt(
	struct ulclog_udp_logger *ulclog_udp_logger,
	int level,
	int optname,
	const void *optval,
	socklen_t optlen)
{
	return setsockopt(ulclog_udp_logger->s,
			  level,
			  optname,
			  optval,
			  optlen);
}

int ulclog_udp_getsockopt(
	struct ulclog_udp_logger *ulclog_udp_logger,
	int level,
	int optname,
	void *optval,
	socklen_t *optlen)
{
	return getsockopt(ulclog_udp_logger->s,
			  level,
			  optname,
			  optval,
			  optlen);
}
