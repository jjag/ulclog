/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include "ulclog/ulclog_config.h"

#include <string.h>
#include <errno.h>
#include <stddef.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ulclog/ulclog_logger_septhread.h"

#define LOG_MODULE "ulclog_septhread"
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_WARNING)
#include "ulclog/ulclog.h"

static int ulclog_septhread_vctor(
	struct ulclog_logger *,
	va_list);
static void ulclog_septhread_dtor(
	struct ulclog_logger *);
static ssize_t ulclog_septhread_dolog(
	struct ulclog_logger *,
	int,
	const char *,
	ssize_t);

struct LoggerClass SepThreadLogger = {
	.size = sizeof(struct ulclog_septhread_logger),
	.vctor = ulclog_septhread_vctor,
	.dtor  = ulclog_septhread_dtor,
	.dolog = ulclog_septhread_dolog,
};

#define MAIN_QNAME "/septhread_queue"

static void *septhread_start_routine(void *);

enum septhread_message_type {
	SEPTHREAD_COMMAND,
	SEPTHREAD_MESSAGE,
};

enum septhread_command {
	SEPTHREAD_COMMAND_STOP,
};

/**
 * \struct septhread_message
 * Structure of the messaged sent from the logger to the thread that
 * will really do the logs.
 */
struct septhread_message {
	/** type of the message */
	enum septhread_message_type type;
	/** length of the message */
	size_t length;
	union {
		/** the command if type is COMMAND  */
		enum septhread_command command;
		struct log_message {
			/** a sequence number to detect missed logs */
			unsigned int seq_number;
			/** log priority */
			int priority;
			/** the log message */
			char message[LOG_BUFFERS_SIZE];
		} log_message;
	};
};

int ulclog_septhread_init(
	struct ulclog_septhread_logger *ulclog_septhread_logger,
	struct ulclog_logger *sublogger,
	const pthread_attr_t *attr,
	long mq_maxmsg,
	missed_msg_cb_t missed_msg_cb)
{
	ulclog_logger_init(&ulclog_septhread_logger->ulclog_logger,
			  &SepThreadLogger);

	struct mq_attr mq_attr = {
		.mq_flags = O_NONBLOCK,
		.mq_maxmsg = mq_maxmsg,
		.mq_msgsize = sizeof(struct septhread_message),
		.mq_curmsgs = 0,
	};
	int pthread_create_error;
	pthread_attr_t forced_attr;

	ulclog_septhread_logger->sublogger = sublogger;
	ulclog_septhread_logger->missed_msg_cb = missed_msg_cb;
	atomic_init(&ulclog_septhread_logger->seq_number, 0);

	mq_unlink(MAIN_QNAME);

	ulclog_septhread_logger->mqdes = mq_open(MAIN_QNAME,
						O_WRONLY | O_CREAT,
						S_IRWXU | S_IRWXG,
						&mq_attr);

	if (ulclog_septhread_logger->mqdes == -1) {
		LERROR("mq_open failed (errno %d) %s", errno, strerror(errno));
		return -1;
	}

	if (mq_setattr(ulclog_septhread_logger->mqdes, &mq_attr, NULL) == -1) {
		LERROR("mq_setattr failed (errno %d) %s",
		       errno, strerror(errno));
		mq_close(ulclog_septhread_logger->mqdes);
		mq_unlink(MAIN_QNAME);
		return -1;
	}

	if (attr) {
		int detachstate;

		memcpy(&forced_attr, attr, sizeof(forced_attr));

		pthread_attr_getdetachstate(&forced_attr, &detachstate);
		if (detachstate != PTHREAD_CREATE_JOINABLE) {
			LWARNING("Bad detachstate (%d): forced to JOINABLE",
				 detachstate);
			pthread_attr_setdetachstate(&forced_attr,
						    PTHREAD_CREATE_JOINABLE);
		}
	}
	pthread_create_error = pthread_create(
		&ulclog_septhread_logger->thread,
		(attr == NULL) ? NULL : &forced_attr,
		septhread_start_routine,
		ulclog_septhread_logger);
	if (pthread_create_error != 0) {
		LERROR("pthread_create failed (error %d) %s - %s",
		       pthread_create_error,
		       (pthread_create_error == EAGAIN) ? "EAGAIN" :
		       (pthread_create_error == EINVAL) ? "EINVAL" :
		       (pthread_create_error == EPERM)  ? "EPERM" :
		       "Unknown", strerror(pthread_create_error));
		if (mq_close(ulclog_septhread_logger->mqdes) == -1)
			LERROR("mq_close failed (errno %d) %s",
			       errno, strerror(errno));
		if (mq_unlink(MAIN_QNAME) == -1)
			LERROR("mq_unlink failed (errno %d) %s",
			       errno, strerror(errno));
		return -1;
	}

	return 0;
}

int ulclog_septhread_vctor(
	struct ulclog_logger *ulclog_logger,
	va_list pa)
{
	struct ulclog_logger *sublogger;
	const pthread_attr_t *attr;
	long mq_maxmsg;
	missed_msg_cb_t missed_msg_cb;

	sublogger = va_arg(pa, struct ulclog_logger *);
	attr = va_arg(pa, const pthread_attr_t *);
	mq_maxmsg = va_arg(pa, long);
	missed_msg_cb = va_arg(pa, missed_msg_cb_t);

	LDEBUG("sublogger [%p] attr [%p] mq_maxmsg [%ld] missed_msg_cb [0x%p]",
	       sublogger, attr, mq_maxmsg, missed_msg_cb);

	return ulclog_septhread_init(
		(struct ulclog_septhread_logger *)ulclog_logger,
		sublogger, attr, mq_maxmsg, missed_msg_cb);
}

void ulclog_septhread_clean(
	struct ulclog_septhread_logger *ulclog_septhread_logger)
{
	struct septhread_message stop_command = {
		.type = SEPTHREAD_COMMAND,
		.length = sizeof(stop_command.command),
		.command = SEPTHREAD_COMMAND_STOP,
	};
	mqd_t mqdes_command;
	struct mq_attr mq_attr = {
		.mq_flags = 0,
	};

	mqdes_command = mq_open(MAIN_QNAME, O_WRONLY);
	if (mqdes_command == -1) {
		LERROR("mq_open failed (errno %d) %s",
		       errno, strerror(errno));
		mq_close(ulclog_septhread_logger->mqdes);
		mq_unlink(MAIN_QNAME);
		return;
	}

	if (mq_setattr(mqdes_command, &mq_attr, NULL) == -1) {
		LERROR("mq_setattr failed (errno %d) %s",
		       errno, strerror(errno));
		mq_close(ulclog_septhread_logger->mqdes);
		mq_close(mqdes_command);
		mq_unlink(MAIN_QNAME);
		return;
	}

	mq_send(mqdes_command,
		(const char *)&stop_command,
		offsetof(struct septhread_message, command)
		+ sizeof(stop_command.command),
		0);

	pthread_join(ulclog_septhread_logger->thread, NULL);

	mq_close(ulclog_septhread_logger->mqdes);
	mq_close(mqdes_command);

	mq_unlink(MAIN_QNAME);
}

void ulclog_septhread_dtor(
	struct ulclog_logger *ulclog_logger)
{
	ulclog_septhread_clean((struct ulclog_septhread_logger *)ulclog_logger);
}

static ssize_t ulclog_septhread_dolog(
	struct ulclog_logger *ulclog_logger,
	int priority,
	const char *message,
	ssize_t nbchar)
{
	struct ulclog_septhread_logger *ulclog_septhread_logger =
		(struct ulclog_septhread_logger *) ulclog_logger;
	struct septhread_message log_command = {
		.type = SEPTHREAD_MESSAGE,
		.length = (unsigned)offsetof(struct log_message, message)
		+ (unsigned)nbchar + 1,
		.log_message.message = { '\0' },
		.log_message.priority = priority,
	};
	int mq_send_return_value;

	strncpy(log_command.log_message.message, message,
		sizeof(log_command.log_message.message) - 1);
	log_command.log_message.seq_number =
		atomic_fetch_add_explicit(&ulclog_septhread_logger->seq_number,
					  1,
					  memory_order_relaxed);

	mq_send_return_value = mq_send(
		ulclog_septhread_logger->mqdes,
		(const char *)&log_command,
		(unsigned)offsetof(struct septhread_message,
				   log_message.message)
		+ (unsigned)nbchar + 1,
		0);

	if (mq_send_return_value != 0) {
		if (errno != EAGAIN)
			LERROR("mq_send failed (errno %d) %s",
			       errno, strerror(errno));
		return 0;
	}

	return nbchar;
}

void *septhread_start_routine(
	void *arg)
{
	struct ulclog_septhread_logger *ulclog_septhread_logger = arg;
	mqd_t mqdes;
	struct mq_attr mq_attr = {
		.mq_flags = 0,
	};
	struct septhread_message message;
	unsigned int expected_seq_number = 0;

	mqdes = mq_open(MAIN_QNAME, O_RDONLY);
	if (mqdes == -1) {
		LERROR("mq_open failed (errno %d) %s",
		       errno, strerror(errno));
		return NULL;
	}

	if (mq_setattr(mqdes, &mq_attr, NULL) == -1) {
		LERROR("mq_setattr failed (errno %d) %s",
		       errno, strerror(errno));
		return NULL;
	}

	while (1) {
		ssize_t message_length;

		message_length = mq_receive(mqdes,
					    (char *)&message,
					    sizeof(message),
					    NULL);

		if (message_length == -1) {
			LERROR("mq_receive failed (errno %d) %s",
			       errno, strerror(errno));
			continue;
		}

		switch (message.type) {
		case SEPTHREAD_COMMAND:{
			switch (message.command) {
			case SEPTHREAD_COMMAND_STOP:{
				if (message_length !=
				    offsetof(struct septhread_message, command)
				    + sizeof(message.command)) {
					LERROR("Bad command length(%zd , %zd)",
					       message_length,
					       offsetof(
						       struct septhread_message,
						       command)
					       + sizeof(message.command));
					break;
				}
				if (message.length !=
				    sizeof(message.command)) {
					LERROR("Bad message.length (%zd)",
					       message.length);
					break;
				}
				return NULL;
			}
			default:{
				LERROR("Bad command (%d)",
				       message.command);
				break;
			}
			}
			break;
		}
		case SEPTHREAD_MESSAGE:{
			if ((unsigned)message_length !=
			    offsetof(struct septhread_message,
				     log_message)
			    + message.length) {
				LERROR("Bad message_length (%zd %zd %zd)",
				       message_length,
				       offsetof(struct septhread_message,
						log_message),
				       message.length);
				break;
			}

			if ((expected_seq_number !=
			     message.log_message.seq_number)
			    && (ulclog_septhread_logger->missed_msg_cb))
				ulclog_septhread_logger->missed_msg_cb(
					ulclog_septhread_logger->sublogger,
					message.log_message.seq_number
					- expected_seq_number);

			expected_seq_number =
				message.log_message.seq_number + 1;

			ulclog_logger_log_noformat(
				ulclog_septhread_logger->sublogger,
				message.log_message.priority,
				message.log_message.message);
			break;
		}
		default:
			LERROR("Bad message type (%d)", message.type);
			break;
		}
	}
}
