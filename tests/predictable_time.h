/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#ifndef PREDICTABLE_TIME_H
#define PREDICTABLE_TIME_H

#define LOG_MODULE	"tests"
#include "ulclog/ulclog.h"

/* make time predictible */
#if HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY

#include <sys/time.h>

static struct timeval ref = {1000, 999999};

int gettimeofday(
	struct timeval *tv,
	void *tz __attribute__ ((unused)))
{
	*tv = ref;
	ref.tv_sec++;
	ref.tv_usec--;
	return 0;
}

void reset_gettimeofday_ref(void);
void reset_gettimeofday_ref(void)
{
	ref.tv_sec  = 1000;
	ref.tv_usec = 999999;
}

#else /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */

#include <time.h>
time_t time(
	time_t *t __attribute__ ((unused)))
{
	static time_t time;

	time += 1;
	return time;

}
#endif /* HAVE_SYS_TIME_H && HAVE_GETTIMEOFDAY */

#endif /* PREDICTABLE_TIME_H */
