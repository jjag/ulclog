/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <check.h>

#include "predictable_time.h"

static void setup(void);

struct Suite *get_suite(void);

#include "ulclog/ulclog_logger.h"

struct ulclog_test_logger {
	struct ulclog_logger ulclog_logger;
	int priority;
	char message[LOG_BUFFERS_SIZE];
	ssize_t dolog_return_value;
};

static int ulclog_test_vctor(
	struct ulclog_logger *,
	va_list);
static void ulclog_test_dtor(
	struct ulclog_logger *);
static ssize_t ulclog_test_dolog(
	struct ulclog_logger *,
	int,
	const char *,
	ssize_t);

static struct LoggerClass TestLogger = {
	.size = sizeof(struct ulclog_test_logger),
	.vctor = ulclog_test_vctor,
	.dtor  = ulclog_test_dtor,
	.dolog = ulclog_test_dolog,
};

int ulclog_test_vctor(
	struct ulclog_logger *ulclog_logger,
	va_list pa __attribute__((unused)))
{
	struct ulclog_test_logger *ulclog_test_logger =
		(struct ulclog_test_logger *)ulclog_logger;
	ulclog_test_logger->priority = -1;
	ulclog_test_logger->message[0] = '\0';
	ulclog_test_logger->dolog_return_value = 0;

	return 0;
}

void ulclog_test_dtor(
	struct ulclog_logger *ulclog_logger __attribute__((unused)))
{
}

static ssize_t ulclog_test_dolog(
	struct ulclog_logger *ulclog_logger,
	int priority,
	const char *message,
	ssize_t nbchar __attribute__((unused)))
{
	struct ulclog_test_logger *ulclog_test_logger =
		(struct ulclog_test_logger *)ulclog_logger;
	ulclog_test_logger->priority = priority;
	strncpy(ulclog_test_logger->message, message,
		sizeof(ulclog_test_logger->message) - 1);

	return ulclog_test_logger->dolog_return_value;
}

#include "ulclog/ulclog_logger_septhread.h"

/* mq_unlink stub */
static char mq_unlink_stub_name[1024];
static int mq_unlink_stub_return_value;
int mq_unlink(
	const char *name)
{
	strncpy(mq_unlink_stub_name, name, sizeof(mq_unlink_stub_name) - 1);

	return mq_unlink_stub_return_value;
}

/* mq_open stub */
static int mq_open_stub_ncall;
static char mq_open_stub_name[10][1024];
static int mq_open_stub_oflag[10];
static mode_t mq_open_stub_mode;
static struct mq_attr mq_open_stub_attr;
static mqd_t mq_open_stub_return_value[10];
mqd_t mq_open(
	const char *name,
	int oflag,
	...)
{
	int ncall = mq_open_stub_ncall++;

	strncpy(mq_open_stub_name[ncall], name,
		sizeof(mq_open_stub_name[ncall]));
	mq_open_stub_oflag[ncall] = oflag;

	if (oflag & O_CREAT) {
		va_list pa;
		struct mq_attr *attr;

		va_start(pa, oflag);

		mq_open_stub_mode = va_arg(pa, mode_t);
		attr = va_arg(pa, struct mq_attr *);

		va_end(pa);
		memcpy(&mq_open_stub_attr, attr, sizeof(mq_open_stub_attr));
	}

	return mq_open_stub_return_value[ncall];
}

/* mq_close stub */
static int mq_close_stub_ncall;
static mqd_t mq_close_stub_mqdes[10];
static mqd_t mq_close_stub_return_value[10];
int mq_close(
	mqd_t mqdes)
{
	int ncall = mq_close_stub_ncall++;

	mq_close_stub_mqdes[ncall] = mqdes;

	return mq_close_stub_return_value[ncall];
}

struct log_message {
	enum { TYPE_MESSAGE = 1 } type;
	size_t length;
	unsigned int seq_number;
	int priority;
	char message[LOG_BUFFERS_SIZE];
} log_message;


/* mq_send stub */
static mqd_t mq_send_stub_mqdes;
static char mq_send_stub_msg[sizeof(struct log_message)];
static size_t mq_send_stub_msg_len;
static unsigned int mq_send_stub_msg_prio;
static int mq_send_stub_return_value;
int mq_send(
	mqd_t mqdes,
	const char *msg_ptr,
	size_t msg_len,
	unsigned int msg_prio)
{
	mq_send_stub_mqdes = mqdes;
	memcpy(mq_send_stub_msg, msg_ptr, msg_len);
	mq_send_stub_msg_len = msg_len;
	mq_send_stub_msg_prio = msg_prio;

	return mq_send_stub_return_value;
}

/* man mq_receive stub */
static int mq_receive_stub_ncall;
static mqd_t mq_receive_stub_mqdes[10];
static char *mq_receive_stub_msg[10];
static size_t mq_receive_stub_msg_len[10];
static unsigned int mq_receive_stub_msg_prio[10];
static ssize_t mq_receive_stub_return_value[10];
ssize_t mq_receive(
	mqd_t mqdes,
	char *msg_ptr,
	size_t msg_len,
	unsigned int *msg_prio)
{
	int ncall = mq_receive_stub_ncall++;

	mq_receive_stub_mqdes[ncall] = mqdes;
	memcpy(msg_ptr, mq_receive_stub_msg[ncall], msg_len);
	mq_receive_stub_msg_len[ncall] = msg_len;
	if (msg_prio)
		*msg_prio = mq_receive_stub_msg_prio[ncall];

	return mq_receive_stub_return_value[ncall];
}

/* mq_setattr stub */
static int mq_setattr_stub_ncall;
static mqd_t mq_setattr_stub_mqdes[10];
static long mq_setattr_stub_newattr_mq_flags[10];
static struct mq_attr *mq_setattr_stub_oldattr[10];
int mq_setattr_stub_return_value[10];
int mq_setattr(
	mqd_t mqdes,
	const struct mq_attr *newattr,
	struct mq_attr *oldattr)
{
	int ncall = mq_setattr_stub_ncall++;

	mq_setattr_stub_mqdes[ncall] = mqdes;
	mq_setattr_stub_newattr_mq_flags[ncall] = newattr->mq_flags;
	mq_setattr_stub_oldattr[ncall] = oldattr;

	return mq_setattr_stub_return_value[ncall];
}

/* pthread_create stub */
static pthread_t pthread_create_stub_thread_value;
static pthread_attr_t pthread_create_stub_attr;
static const pthread_attr_t *pthread_create_stub_attr_pointer;
static void *(*pthread_create_stub_start_routine)(void *);
static int pthread_create_stub_call_start_routine;
static void *pthread_create_stub_arg;
static int pthread_create_stub_return_value;
int pthread_create(
	pthread_t *thread,
	const pthread_attr_t *attr,
	void *(*start_routine)(void *),
	void *arg)
{
	*thread = pthread_create_stub_thread_value;
	pthread_create_stub_attr_pointer = attr;
	if (attr)
		memcpy(&pthread_create_stub_attr, attr,
		       sizeof(pthread_create_stub_attr));
	pthread_create_stub_start_routine = start_routine;
	pthread_create_stub_arg = arg;

	if (pthread_create_stub_call_start_routine)
		pthread_create_stub_start_routine(arg);

	return pthread_create_stub_return_value;
}


/* pthread_join stub */
pthread_t pthread_join_stub_thread;
void **pthread_join_stub_retval;
int pthread_join_stub_return_value;
int pthread_join(
	pthread_t thread,
	void **retval)
{
	pthread_join_stub_thread = thread;
	pthread_join_stub_retval = retval;

	return pthread_join_stub_return_value;
}

static struct ulclog_logger *missed_msg_cb_logger;
static unsigned int missed_msg_cb_nmissed;
static struct ulclog_logger *fake_logger;

void setup(void)
{
	fake_logger = (struct ulclog_logger *)0x12345678;

	mq_unlink_stub_name[0] = '\0';
	mq_unlink_stub_return_value = 0;

	mq_open_stub_ncall = 0;
	for (int i = 0; i < 10 ; i++) {
		mq_open_stub_name[i][0] = '\0';
		mq_open_stub_oflag[i] = 0;
		mq_open_stub_return_value[i] = 0;
	}
	mq_open_stub_mode = 0;
	memset(&mq_open_stub_attr, 0, sizeof(mq_open_stub_attr));

	mq_close_stub_ncall = 0;
	for (int i = 0; i < 10 ; i++) {
		mq_close_stub_mqdes[i] = 0;
		mq_close_stub_return_value[i] = 0;
	}

	mq_send_stub_mqdes = 0;
	mq_send_stub_msg[0] = '\0';
	mq_send_stub_msg_len = 0;
	mq_send_stub_msg_prio = UINT32_MAX;
	mq_send_stub_return_value = 0;

	mq_receive_stub_ncall = 0;
	for (int i = 0; i < 10 ; i++) {
		mq_receive_stub_mqdes[i] = 0;
		mq_receive_stub_msg[i] = (void *)-1;
		mq_receive_stub_msg_len[i] = 0;
		mq_receive_stub_msg_prio[i] = 0;
		mq_receive_stub_return_value[i] = 0;
	}

	mq_setattr_stub_ncall = 0;
	for (int i = 0; i < 10 ; i++) {
		mq_setattr_stub_mqdes[i] = -1;
		mq_setattr_stub_newattr_mq_flags[i] = -1;
		mq_setattr_stub_oldattr[i] = (void *)-1;
		mq_setattr_stub_return_value[i] = 0;
	}

	memset(&pthread_create_stub_thread_value, 0,
	       sizeof(pthread_create_stub_thread_value));
	memset(&pthread_create_stub_attr, 0,
	       sizeof(pthread_create_stub_attr));
	pthread_create_stub_attr_pointer = (void *)-1;
	pthread_create_stub_start_routine = (void *)-1;
	pthread_create_stub_call_start_routine = 0;
	pthread_create_stub_arg = (void *)-1;
	pthread_create_stub_return_value = 0;

	memset(&pthread_join_stub_thread, 0, sizeof(pthread_join_stub_thread));
	pthread_join_stub_retval = (void *)-1;
	pthread_join_stub_return_value = 0;

	missed_msg_cb_logger = NULL;
	missed_msg_cb_nmissed = 0;
}

START_TEST(test_septhread_init_clean)
{
	struct ulclog_septhread_logger logger;
	int init_return_value;

	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0; /* no error */
	mq_open_stub_return_value[0] = 189; /* mq_desc */

	init_return_value = ulclog_septhread_init(&logger,
						 fake_logger,
						 NULL,
						 10,
						 NULL);

	ck_assert_int_eq(init_return_value, 0);
	ck_assert_str_eq(mq_unlink_stub_name, "/septhread_queue");
	ck_assert_int_eq(mq_open_stub_ncall, 1);
	ck_assert_str_eq(mq_open_stub_name[0], "/septhread_queue");
	ck_assert_int_eq(mq_open_stub_oflag[0], O_WRONLY | O_CREAT);
	ck_assert_int_eq(mq_open_stub_mode, S_IRWXU | S_IRWXG);
	ck_assert_int_eq(mq_open_stub_attr.mq_flags, O_NONBLOCK);
	ck_assert_int_eq(mq_open_stub_attr.mq_maxmsg, 10);

	ck_assert_ptr_eq(pthread_create_stub_attr_pointer, NULL);
	ck_assert_ptr_ne(pthread_create_stub_start_routine, NULL);
	ck_assert_ptr_eq(pthread_create_stub_arg, &logger);

	setup();
	mq_open_stub_return_value[0] = 190; /* mq_desc */

	ulclog_septhread_clean(&logger);

	struct stop_message {
		enum { TYPE_STOP = 0 } type;
		size_t length;
		enum { COMMAND_STOP = 0} command;
	} stop_message;

	ck_assert_int_eq(mq_setattr_stub_ncall, 1);
	ck_assert_int_eq(mq_setattr_stub_mqdes[0], 190);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[0], 0);
	ck_assert_ptr_eq(mq_setattr_stub_oldattr[0], NULL);

	ck_assert_int_eq(mq_send_stub_mqdes, 190);
	memcpy(&stop_message, mq_send_stub_msg, sizeof(stop_message));
	ck_assert_int_eq(stop_message.type, TYPE_STOP);
	ck_assert_int_eq((signed)stop_message.length,
			 sizeof(stop_message.command));
	ck_assert_int_eq(stop_message.command, COMMAND_STOP);
	ck_assert_int_eq((signed)mq_send_stub_msg_len,
			 offsetof(struct stop_message, command)
			 + sizeof(stop_message.command));
	ck_assert_int_eq(mq_send_stub_msg_prio, 0);

	ck_assert_int_eq((signed)pthread_join_stub_thread, 234);
	ck_assert_ptr_eq(pthread_join_stub_retval, NULL);

	ck_assert_str_eq(mq_unlink_stub_name, "/septhread_queue");

	ck_assert_int_eq(mq_close_stub_ncall, 2);
	ck_assert_int_eq(mq_close_stub_mqdes[0], 189);
	ck_assert_int_eq(mq_close_stub_mqdes[1], 190);
}
END_TEST

START_TEST(test_septhread_new_delete)
{
	struct ulclog_logger *logger;

	mq_open_stub_return_value[0] = 189; /* mq_desc */

	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0; /* no error */

	logger = ulclog_logger_new(&SepThreadLogger,
				  fake_logger,
				  NULL,
				  10,
				  NULL);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_str_eq(mq_unlink_stub_name, "/septhread_queue");
	ck_assert_int_eq(mq_open_stub_ncall, 1);
	ck_assert_str_eq(mq_open_stub_name[0], "/septhread_queue");
	ck_assert_int_eq(mq_open_stub_oflag[0], O_WRONLY | O_CREAT);
	ck_assert_int_eq(mq_open_stub_mode, S_IRWXU | S_IRWXG);
	ck_assert_int_eq(mq_open_stub_attr.mq_flags, O_NONBLOCK);
	ck_assert_int_eq(mq_open_stub_attr.mq_maxmsg, 10);

	ck_assert_ptr_eq(pthread_create_stub_attr_pointer, NULL);
	ck_assert_ptr_ne(pthread_create_stub_start_routine, NULL);
	ck_assert_ptr_eq(pthread_create_stub_arg, logger);

	setup();
	mq_open_stub_return_value[0] = 190; /* mq_desc */

	ulclog_logger_delete(logger);

	struct stop_message {
		enum { TYPE_STOP = 0 } type;
		size_t length;
		enum { COMMAND_STOP = 0} command;
	} stop_message;

	ck_assert_int_eq(mq_setattr_stub_ncall, 1);
	ck_assert_int_eq(mq_setattr_stub_mqdes[0], 190);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[0], 0);
	ck_assert_ptr_eq(mq_setattr_stub_oldattr[0], NULL);

	ck_assert_int_eq(mq_send_stub_mqdes, 190);
	memcpy(&stop_message, mq_send_stub_msg, sizeof(stop_message));
	ck_assert_int_eq(stop_message.type, TYPE_STOP);
	ck_assert_int_eq((signed)stop_message.length,
			 sizeof(stop_message.command));
	ck_assert_int_eq(stop_message.command, COMMAND_STOP);
	ck_assert_int_eq((signed)mq_send_stub_msg_len,
			 offsetof(struct stop_message, command)
			 + sizeof(stop_message.command));
	ck_assert_int_eq(mq_send_stub_msg_prio, 0);

	ck_assert_int_eq((signed)pthread_join_stub_thread, 234);
	ck_assert_ptr_eq(pthread_join_stub_retval, NULL);

	ck_assert_str_eq(mq_unlink_stub_name, "/septhread_queue");

	ck_assert_int_eq(mq_close_stub_ncall, 2);
	ck_assert_int_eq(mq_close_stub_mqdes[0], 189);
	ck_assert_int_eq(mq_close_stub_mqdes[1], 190);
}
END_TEST

START_TEST(test_septhread_failed_init_mq_open)
{
	struct ulclog_septhread_logger logger;
	int init_return_value;

	mq_open_stub_return_value[0] = -1; /* mq_desc */

	init_return_value = ulclog_septhread_init(&logger,
						 fake_logger,
						 NULL,
						 10,
						 NULL);

	ck_assert_int_eq(init_return_value, -1);
}
END_TEST

START_TEST(test_septhread_failed_init_pthread_create)
{
	struct ulclog_septhread_logger logger;
	int init_return_value;

	mq_open_stub_return_value[0] = 189;
	pthread_create_stub_return_value = 12;
	mq_unlink_stub_return_value = -1;
	mq_close_stub_return_value[0] = -1;

	init_return_value = ulclog_septhread_init(&logger,
						 fake_logger,
						 NULL,
						 10,
						 NULL);

	ck_assert_int_eq(init_return_value, -1);
	ck_assert_str_eq(mq_unlink_stub_name, "/septhread_queue");
	ck_assert_int_eq(mq_close_stub_ncall, 1);
	ck_assert_int_eq(mq_close_stub_mqdes[0], 189);

	setup();
	mq_open_stub_return_value[0] = 189;
	pthread_create_stub_return_value = EAGAIN;

	init_return_value = ulclog_septhread_init(&logger,
						 fake_logger,
						 NULL,
						 10,
						 NULL);

	ck_assert_int_eq(init_return_value, -1);
	ck_assert_str_eq(mq_unlink_stub_name, "/septhread_queue");
	ck_assert_int_eq(mq_close_stub_ncall, 1);
	ck_assert_int_eq(mq_close_stub_mqdes[0], 189);

	setup();
	mq_open_stub_return_value[0] = 189;
	pthread_create_stub_return_value = EINVAL;

	init_return_value = ulclog_septhread_init(&logger,
						 fake_logger,
						 NULL,
						 10,
						 NULL);

	ck_assert_int_eq(init_return_value, -1);
	ck_assert_str_eq(mq_unlink_stub_name, "/septhread_queue");
	ck_assert_int_eq(mq_close_stub_ncall, 1);
	ck_assert_int_eq(mq_close_stub_mqdes[0], 189);

	setup();
	mq_open_stub_return_value[0] = 189;
	pthread_create_stub_return_value = EPERM;

	init_return_value = ulclog_septhread_init(&logger,
						 fake_logger,
						 NULL,
						 10,
						 NULL);

	ck_assert_int_eq(init_return_value, -1);
	ck_assert_str_eq(mq_unlink_stub_name, "/septhread_queue");
	ck_assert_int_eq(mq_close_stub_ncall, 1);
	ck_assert_int_eq(mq_close_stub_mqdes[0], 189);
}
END_TEST

START_TEST(test_septhread_failed_clean_mq_open)
{
	struct ulclog_septhread_logger logger;

	mq_open_stub_return_value[0] = 189; /* mq_desc */
	pthread_create_stub_return_value = 0; /* no error */

	ulclog_septhread_init(&logger,
			     fake_logger,
			     NULL,
			     10,
			     NULL);

	setup();
	mq_open_stub_return_value[0] = -1;

	ulclog_septhread_clean(&logger);

	ck_assert_int_eq(mq_open_stub_ncall, 1);
	ck_assert_str_eq(mq_open_stub_name[0], "/septhread_queue");
	ck_assert_int_eq(mq_close_stub_ncall, 1);
	ck_assert_int_eq(mq_close_stub_mqdes[0], 189);
	ck_assert_int_eq(mq_setattr_stub_ncall, 0);
}
END_TEST

START_TEST(test_septhread_failed_clean_mq_setattr)
{
	struct ulclog_septhread_logger logger;

	mq_open_stub_return_value[0] = 189; /* mq_desc */
	pthread_create_stub_return_value = 0; /* no error */

	ulclog_septhread_init(&logger,
			     fake_logger,
			     NULL,
			     10,
			     NULL);

	setup();
	mq_open_stub_return_value[0] = 189;
	mq_setattr_stub_return_value[0] = -1;
	mq_setattr_stub_return_value[1] = -1;

	ulclog_septhread_clean(&logger);

	ck_assert_int_eq(mq_open_stub_ncall, 1);
	ck_assert_str_eq(mq_open_stub_name[0], "/septhread_queue");
	ck_assert_int_eq(mq_setattr_stub_ncall, 1);
	ck_assert_int_eq(mq_setattr_stub_mqdes[0], 189);
}
END_TEST

START_TEST(test_septhread_init_with_thread_attr)
{
	struct ulclog_septhread_logger logger;
	int init_return_value;
	pthread_attr_t attr;
	int used_detachstate;

	pthread_attr_init(&attr);

	init_return_value = ulclog_septhread_init(&logger,
						 fake_logger,
						 &attr,
						 10,
						 NULL);

	ck_assert_int_eq(init_return_value, 0);
	ck_assert_ptr_ne(pthread_create_stub_attr_pointer, NULL);

	pthread_attr_getdetachstate(&pthread_create_stub_attr,
				    &used_detachstate);
	ck_assert_int_eq(used_detachstate, PTHREAD_CREATE_JOINABLE);

	pthread_attr_destroy(&attr);
	ulclog_septhread_clean(&logger);
}
END_TEST

START_TEST(test_septhread_init_with_bad_thread_attr)
{
	struct ulclog_septhread_logger logger;
	int init_return_value;
	pthread_attr_t attr;
	int used_detachstate;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	init_return_value = ulclog_septhread_init(&logger,
						 fake_logger,
						 &attr,
						 10,
						 NULL);

	ck_assert_int_eq(init_return_value, 0);
	ck_assert_ptr_ne(pthread_create_stub_attr_pointer, NULL);

	pthread_attr_getdetachstate(&pthread_create_stub_attr,
				    &used_detachstate);
	ck_assert_int_eq(used_detachstate, PTHREAD_CREATE_JOINABLE);

	pthread_attr_destroy(&attr);
	ulclog_septhread_clean(&logger);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#define LOG_METHOD		ulclog_logger_log
#define LOG_CONTEXT(level)	LOG_PARAMS(logger, level)
#define LOG_MODULE		"test_logger"
#define PRE_FORMAT		"Tadaaam!"
#define POST_FORMAT		""

#define LOG(level, format, ...)				\
	LOG_METHOD(LOG_CONTEXT(level),			\
		   PRE_FORMAT format POST_FORMAT,	\
		   ##__VA_ARGS__)

#include "ulclog/ulclog.h"
START_TEST(test_septhread_log)
{
	struct ulclog_logger *logger;
	ssize_t nbchar;

	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;

	logger = ulclog_logger_new(&SepThreadLogger,
				  fake_logger,
				  NULL,
				  10,
				  NULL);

	ck_assert_ptr_ne(logger, NULL);

	memset(mq_send_stub_msg, 0, sizeof(mq_send_stub_msg));
	mq_send_stub_mqdes = 0;
	mq_send_stub_msg_len = 0;
	mq_send_stub_msg_prio = UINT32_MAX;
	mq_send_stub_return_value = 0;

	nbchar = LWARNING("WARNING");

	ck_assert_int_eq(nbchar, 15);
	ck_assert_int_eq(mq_send_stub_mqdes, 189);
	memcpy(&log_message, mq_send_stub_msg, sizeof(log_message));
	ck_assert_int_eq(log_message.type, TYPE_MESSAGE);
	ck_assert_int_eq((signed)log_message.length,
			 (signed)(offsetof(struct log_message, message)
				  - offsetof(struct log_message, seq_number)
				  + strlen(log_message.message) + 1));
	ck_assert_int_eq(log_message.seq_number, 0);
	ck_assert_int_eq(log_message.priority, LOG_WARNING);
	ck_assert_str_eq(log_message.message, "Tadaaam!WARNING");
	ck_assert_int_eq((signed)mq_send_stub_msg_len,
			 (signed)(offsetof(struct log_message, message)
				  + strlen(log_message.message) + 1));
	ck_assert_int_eq(mq_send_stub_msg_prio, 0);

	nbchar = LERROR("ERROR");

	ck_assert_int_eq(nbchar, 13);
	ck_assert_int_eq(mq_send_stub_mqdes, 189);
	memcpy(&log_message, mq_send_stub_msg, sizeof(log_message));
	ck_assert_int_eq(log_message.type, TYPE_MESSAGE);
	ck_assert_int_eq((signed)log_message.length,
			 (signed)(offsetof(struct log_message, message)
				  - offsetof(struct log_message, seq_number)
				  + strlen(log_message.message) + 1));
	ck_assert_int_eq(log_message.seq_number, 1);
	ck_assert_int_eq(log_message.priority, LOG_ERR);
	ck_assert_str_eq(log_message.message, "Tadaaam!ERROR");
	ck_assert_int_eq((signed)mq_send_stub_msg_len,
			 (signed)(offsetof(struct log_message, message)
				  + strlen(log_message.message) + 1));
	ck_assert_int_eq(mq_send_stub_msg_prio, 0);

	ulclog_logger_delete(logger);
}
END_TEST

START_TEST(test_septhread_failed_log)
{
	struct ulclog_logger *logger;
	ssize_t nbchar;

	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;

	logger = ulclog_logger_new(&SepThreadLogger,
				  fake_logger,
				  NULL,
				  10,
				  NULL);

	ck_assert_ptr_ne(logger, NULL);

	mq_send_stub_msg_prio = UINT32_MAX;
	mq_send_stub_return_value = -1;

	nbchar = LWARNING("WARNING");

	ck_assert_int_eq(nbchar, 0);

	ulclog_logger_delete(logger);
}
END_TEST

struct command_message {
	enum { TYPE_STOP = 0 } type;
	size_t length;
	enum { COMMAND_STOP = 0} command;
};
struct command_message stop_message = {
	.type = TYPE_STOP,
	.length = sizeof(stop_message.command),
	.command = COMMAND_STOP,
};

struct log_message log_message1 = {
	.type = TYPE_MESSAGE,
	.length = 0,
	.seq_number = 0,
	.priority = 0,
};

struct log_message log_message2 = {
	.type = TYPE_MESSAGE,
	.length = 0,
	.seq_number = 0,
	.priority = 0,
};

START_TEST(test_septhread_message_rx_stop)
{
	struct ulclog_logger *logger;

	pthread_create_stub_call_start_routine = 1;
	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;
	mq_open_stub_return_value[1] = 310;

	mq_setattr_stub_return_value[0] = 0;
	mq_setattr_stub_return_value[1] = 0;

	mq_receive_stub_msg[0] = (char *)&stop_message;
	mq_receive_stub_return_value[0] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command);

	logger = ulclog_logger_new(&SepThreadLogger,
				  fake_logger,
				  NULL,
				  10,
				  NULL);

	ck_assert_ptr_ne(logger, NULL);

	ulclog_logger_delete(logger);
}
END_TEST

static void missed_msg_cb(struct ulclog_logger *ulclog_logger,
			  unsigned int nmissed);

void missed_msg_cb(struct ulclog_logger *ulclog_logger,
		   unsigned int nmissed)
{
	missed_msg_cb_logger = ulclog_logger;
	missed_msg_cb_nmissed = nmissed;
}

START_TEST(test_septhread_message_rx_log)
{
	struct ulclog_logger *logger;
	struct ulclog_logger *sublogger;

	sublogger = ulclog_logger_new(&TestLogger);

	pthread_create_stub_call_start_routine = 1;
	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;
	mq_open_stub_return_value[1] = 310;

	mq_setattr_stub_mqdes[0] = -1;
	mq_setattr_stub_newattr_mq_flags[0] = -1;
	mq_setattr_stub_return_value[0] = 0;
	mq_setattr_stub_mqdes[1] = -1;
	mq_setattr_stub_newattr_mq_flags[1] = -1;
	mq_setattr_stub_return_value[1] = 0;

	strcpy(log_message1.message, "Tadaaam!WARNING");
	log_message1.length = 8 + 15 + 1;
	mq_receive_stub_msg[0] = (char *)&log_message1;
	mq_receive_stub_return_value[0] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message1.length);

	mq_receive_stub_msg[1] = (char *)&stop_message;
	mq_receive_stub_return_value[1] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command);

	logger = ulclog_logger_new(&SepThreadLogger,
				  sublogger,
				  NULL,
				  10,
				  missed_msg_cb);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_int_eq(mq_receive_stub_ncall, 2);
	ck_assert_int_eq(mq_setattr_stub_mqdes[0], 189);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[0], O_NONBLOCK);
	ck_assert_int_eq(mq_setattr_stub_mqdes[1], 310);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[1], 0);
	ck_assert_int_eq(mq_receive_stub_mqdes[0], 310);
	ck_assert_int_eq(mq_receive_stub_mqdes[1], 310);
	ck_assert_str_eq(((struct ulclog_test_logger *)sublogger)->message,
			 "Tadaaam!WARNING");

	ulclog_logger_delete(logger);
	ulclog_logger_delete(sublogger);
}
END_TEST

START_TEST(test_septhread_message_rx_mq_open_failure)
{
	struct ulclog_logger *logger;
	struct ulclog_logger *sublogger;

	sublogger = ulclog_logger_new(&TestLogger);

	pthread_create_stub_call_start_routine = 1;
	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;
	mq_open_stub_return_value[1] = -1;

	mq_setattr_stub_return_value[0] = 0;
	mq_setattr_stub_return_value[1] = 0;

	strcpy(log_message1.message, "Tadaaam!WARNING");
	log_message1.length = 8 + 15 + 1;
	mq_receive_stub_msg[0] = (char *)&log_message1;
	mq_receive_stub_return_value[0] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message1.length);

	mq_receive_stub_msg[1] = (char *)&stop_message;
	mq_receive_stub_return_value[1] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command);

	logger = ulclog_logger_new(&SepThreadLogger,
				  sublogger,
				  NULL,
				  10,
				  missed_msg_cb);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_int_eq(mq_setattr_stub_ncall, 1);
	ck_assert_int_eq(mq_setattr_stub_mqdes[0], 189);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[0], O_NONBLOCK);
	ck_assert_int_eq(mq_receive_stub_mqdes[0], 0);
	ck_assert_int_eq(mq_receive_stub_mqdes[1], 0);
	ck_assert_str_eq(((struct ulclog_test_logger *)sublogger)->message, "");

	ulclog_logger_delete(logger);
	ulclog_logger_delete(sublogger);
}
END_TEST

START_TEST(test_septhread_message_rx_mq_setattr_failure1)
{
	struct ulclog_logger *logger;
	struct ulclog_logger *sublogger;

	sublogger = ulclog_logger_new(&TestLogger);

	pthread_create_stub_call_start_routine = 1;
	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;
	mq_open_stub_return_value[1] = 310;

	mq_setattr_stub_return_value[0] = -1;

	strcpy(log_message1.message, "Tadaaam!WARNING");
	log_message1.length = 8 + 15 + 1;
	mq_receive_stub_msg[0] = (char *)&log_message1;
	mq_receive_stub_return_value[0] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message1.length);

	mq_receive_stub_msg[1] = (char *)&stop_message;
	mq_receive_stub_return_value[1] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command);

	logger = ulclog_logger_new(&SepThreadLogger,
				  sublogger,
				  NULL,
				  10,
				  missed_msg_cb);

	ck_assert_ptr_eq(logger, NULL);
	ck_assert_int_eq(mq_setattr_stub_ncall, 1);
	ck_assert_int_eq(mq_setattr_stub_mqdes[0], 189);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[0], O_NONBLOCK);
	ck_assert_int_eq(mq_receive_stub_mqdes[0], 0);
	ck_assert_int_eq(mq_receive_stub_mqdes[1], 0);
	ck_assert_str_eq(((struct ulclog_test_logger *)sublogger)->message, "");

	ulclog_logger_delete(sublogger);
}
END_TEST

START_TEST(test_septhread_message_rx_mq_setattr_failure2)
{
	struct ulclog_logger *logger;
	struct ulclog_logger *sublogger;

	sublogger = ulclog_logger_new(&TestLogger);

	pthread_create_stub_call_start_routine = 1;
	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;
	mq_open_stub_return_value[1] = 310;

	mq_setattr_stub_return_value[0] = 0;
	mq_setattr_stub_return_value[1] = -1;

	strcpy(log_message1.message, "Tadaaam!WARNING");
	log_message1.length = 8 + 15 + 1;
	mq_receive_stub_msg[0] = (char *)&log_message1;
	mq_receive_stub_return_value[0] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message1.length);

	mq_receive_stub_msg[1] = (char *)&stop_message;
	mq_receive_stub_return_value[1] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command);

	logger = ulclog_logger_new(&SepThreadLogger,
				  sublogger,
				  NULL,
				  10,
				  missed_msg_cb);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_int_eq(mq_setattr_stub_ncall, 2);
	ck_assert_int_eq(mq_setattr_stub_mqdes[0], 189);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[0], O_NONBLOCK);
	ck_assert_int_eq(mq_setattr_stub_mqdes[1], 310);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[1], 0);
	ck_assert_int_eq(mq_receive_stub_mqdes[0], 0);
	ck_assert_int_eq(mq_receive_stub_mqdes[1], 0);
	ck_assert_str_eq(((struct ulclog_test_logger *)sublogger)->message, "");

	ulclog_logger_delete(logger);
	ulclog_logger_delete(sublogger);
}
END_TEST

START_TEST(test_septhread_message_rx_mq_receive_failure)
{
	struct ulclog_logger *logger;
	struct ulclog_logger *sublogger;

	sublogger = ulclog_logger_new(&TestLogger);

	pthread_create_stub_call_start_routine = 1;
	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;
	mq_open_stub_return_value[1] = 310;

	mq_setattr_stub_return_value[0] = 0;
	mq_setattr_stub_return_value[1] = 0;

	strcpy(log_message1.message, "Tadaaam!WARNING");
	log_message1.length = 15;
	mq_receive_stub_msg[0] = (char *)&log_message1;
	mq_receive_stub_return_value[0] = -1;

	mq_receive_stub_msg[1] = (char *)&stop_message;
	mq_receive_stub_return_value[1] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command);

	logger = ulclog_logger_new(&SepThreadLogger,
				  sublogger,
				  NULL,
				  10,
				  missed_msg_cb);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_int_eq(mq_setattr_stub_ncall, 2);
	ck_assert_int_eq(mq_setattr_stub_mqdes[0], 189);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[0], O_NONBLOCK);
	ck_assert_int_eq(mq_setattr_stub_mqdes[1], 310);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[1], 0);
	ck_assert_int_eq(mq_receive_stub_mqdes[0], 310);
	ck_assert_int_eq(mq_receive_stub_mqdes[1], 310);
	ck_assert_str_eq(((struct ulclog_test_logger *)sublogger)->message, "");

	ulclog_logger_delete(logger);
	ulclog_logger_delete(sublogger);
}
END_TEST

struct command_message bad_message_type = {
	.type = 10,
	.length = sizeof(bad_message_type.command),
	.command = COMMAND_STOP,
};

struct command_message bad_message_command = {
	.type = TYPE_STOP,
	.length = sizeof(bad_message_type.command),
	.command = 123,
};

struct command_message bad_message_length = {
	.type = TYPE_STOP,
	.length = sizeof(bad_message_type.command) + 10,
	.command = COMMAND_STOP,
};

START_TEST(test_septhread_message_rx_bad_message)
{
	struct ulclog_logger *logger;
	struct ulclog_logger *sublogger;

	sublogger = ulclog_logger_new(&TestLogger);

	pthread_create_stub_call_start_routine = 1;
	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;
	mq_open_stub_return_value[1] = 310;

	mq_setattr_stub_return_value[0] = 0;
	mq_setattr_stub_return_value[1] = 0;

	mq_receive_stub_msg[0] = (char *)&bad_message_type;
	mq_receive_stub_return_value[0] =
		offsetof(struct command_message, command)
		+ sizeof(bad_message_type.command);

	mq_receive_stub_msg[1] = (char *)&bad_message_command;
	mq_receive_stub_return_value[1] =
		offsetof(struct command_message, command)
		+ sizeof(bad_message_type.command);

	mq_receive_stub_msg[2] = (char *)&stop_message;
	mq_receive_stub_return_value[2] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command) + 100;

	mq_receive_stub_msg[3] = (char *)&bad_message_length;
	mq_receive_stub_return_value[3] =
		offsetof(struct command_message, command)
		+ sizeof(bad_message_length.command);

	strcpy(log_message1.message, "Tadaaam!WARNING");
	log_message1.length = 8 + 15 + 1;
	mq_receive_stub_msg[4] = (char *)&log_message1;
	mq_receive_stub_return_value[4] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message1.length + 100);

	mq_receive_stub_msg[5] = (char *)&stop_message;
	mq_receive_stub_return_value[5] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command);

	logger = ulclog_logger_new(&SepThreadLogger,
				  sublogger,
				  NULL,
				  10,
				  missed_msg_cb);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_int_eq(mq_setattr_stub_ncall, 2);
	ck_assert_int_eq(mq_setattr_stub_mqdes[0], 189);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[0], O_NONBLOCK);
	ck_assert_int_eq(mq_setattr_stub_mqdes[1], 310);
	ck_assert_int_eq(mq_setattr_stub_newattr_mq_flags[1], 0);
	ck_assert_int_eq(mq_receive_stub_mqdes[0], 310);
	ck_assert_int_eq(mq_receive_stub_mqdes[1], 310);
	ck_assert_str_eq(((struct ulclog_test_logger *)sublogger)->message, "");
	ck_assert_int_eq(mq_receive_stub_ncall, 6);

	ulclog_logger_delete(logger);
	ulclog_logger_delete(sublogger);
}
END_TEST

START_TEST(test_septhread_message_rx_no_missed_message)
{
	struct ulclog_logger *logger;
	struct ulclog_logger *sublogger;

	sublogger = ulclog_logger_new(&TestLogger);

	pthread_create_stub_call_start_routine = 1;
	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;
	mq_open_stub_return_value[1] = 310;

	mq_setattr_stub_return_value[0] = 0;
	mq_setattr_stub_return_value[1] = 0;

	strcpy(log_message1.message, "Tadaaam!WARNING");
	log_message1.length = 8 + 15 + 1;
	log_message1.seq_number = 0;
	mq_receive_stub_msg[0] = (char *)&log_message1;
	mq_receive_stub_return_value[0] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message1.length);

	strcpy(log_message2.message, "Tadaaam!WARNING");
	log_message2.length = 8 + 15 + 1;
	log_message2.seq_number = 1;
	mq_receive_stub_msg[1] = (char *)&log_message2;
	mq_receive_stub_return_value[1] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message2.length);

	mq_receive_stub_msg[2] = (char *)&stop_message;
	mq_receive_stub_return_value[2] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command);

	logger = ulclog_logger_new(&SepThreadLogger,
				  sublogger,
				  NULL,
				  10,
				  missed_msg_cb);

	ck_assert_ptr_eq(missed_msg_cb_logger, NULL);
	ck_assert_int_eq(missed_msg_cb_nmissed, 0);

	ulclog_logger_delete(logger);
	ulclog_logger_delete(sublogger);
}
END_TEST

START_TEST(test_septhread_message_rx_missed_message)
{
	struct ulclog_logger *logger;
	struct ulclog_logger *sublogger;

	sublogger = ulclog_logger_new(&TestLogger);

	pthread_create_stub_call_start_routine = 1;
	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;
	mq_open_stub_return_value[1] = 310;

	mq_setattr_stub_return_value[0] = 0;
	mq_setattr_stub_return_value[1] = 0;

	strcpy(log_message1.message, "Tadaaam!WARNING");
	log_message1.length = 8 + 15 + 1;
	log_message1.seq_number = 0;
	mq_receive_stub_msg[0] = (char *)&log_message1;
	mq_receive_stub_return_value[0] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message1.length);

	strcpy(log_message2.message, "Tadaaam!WARNING");
	log_message2.length = 8 + 15 + 1;
	log_message2.seq_number = 10;
	mq_receive_stub_msg[1] = (char *)&log_message2;
	mq_receive_stub_return_value[1] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message2.length);

	mq_receive_stub_msg[2] = (char *)&stop_message;
	mq_receive_stub_return_value[2] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command);

	logger = ulclog_logger_new(&SepThreadLogger,
				  sublogger,
				  NULL,
				  10,
				  missed_msg_cb);

	ck_assert_ptr_eq(missed_msg_cb_logger, sublogger);
	ck_assert_int_eq(missed_msg_cb_nmissed, 9);

	ulclog_logger_delete(logger);
	ulclog_logger_delete(sublogger);
}
END_TEST

START_TEST(test_septhread_message_rx_missed_message_wo_cb)
{
	struct ulclog_logger *logger;
	struct ulclog_logger *sublogger;

	sublogger = ulclog_logger_new(&TestLogger);

	pthread_create_stub_call_start_routine = 1;
	pthread_create_stub_thread_value = (pthread_t) 234;
	pthread_create_stub_return_value = 0;
	mq_open_stub_return_value[0] = 189;
	mq_open_stub_return_value[1] = 310;

	mq_setattr_stub_return_value[0] = 0;
	mq_setattr_stub_return_value[1] = 0;

	strcpy(log_message1.message, "Tadaaam!WARNING");
	log_message1.length = 8 + 15 + 1;
	log_message1.seq_number = 0;
	mq_receive_stub_msg[0] = (char *)&log_message1;
	mq_receive_stub_return_value[0] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message1.length);

	strcpy(log_message2.message, "Tadaaam!WARNING");
	log_message2.length = 8 + 15 + 1;
	log_message2.seq_number = 10;
	mq_receive_stub_msg[1] = (char *)&log_message2;
	mq_receive_stub_return_value[1] =
		(signed)(offsetof(struct log_message, seq_number)
			 + log_message2.length);

	mq_receive_stub_msg[2] = (char *)&stop_message;
	mq_receive_stub_return_value[2] =
		offsetof(struct command_message, command)
		+ sizeof(stop_message.command);

	logger = ulclog_logger_new(&SepThreadLogger,
				  sublogger,
				  NULL,
				  10,
				  NULL);

	ck_assert_ptr_eq(missed_msg_cb_logger, NULL);
	ck_assert_int_eq(missed_msg_cb_nmissed, 0);

	ulclog_logger_delete(logger);
	ulclog_logger_delete(sublogger);
}
END_TEST

struct Suite *get_suite(void)
{
	struct Suite *s;
	struct TCase *tc;

	s = suite_create("Separate thread back-end - stubbed");

	/* Core test case */
	tc = tcase_create("Separate thread back-end - stubbed");

	tcase_add_checked_fixture(tc, setup, NULL);

	tcase_add_test(tc, test_septhread_init_clean);
	tcase_add_test(tc, test_septhread_new_delete);
	tcase_add_test(tc, test_septhread_failed_init_mq_open);
	tcase_add_test(tc, test_septhread_failed_init_pthread_create);
	tcase_add_test(tc, test_septhread_failed_clean_mq_open);
	tcase_add_test(tc, test_septhread_failed_clean_mq_setattr);
	tcase_add_test(tc, test_septhread_init_with_bad_thread_attr);
	tcase_add_test(tc, test_septhread_init_with_thread_attr);
	tcase_add_test(tc, test_septhread_log);
	tcase_add_test(tc, test_septhread_failed_log);
	tcase_add_test(tc, test_septhread_message_rx_stop);
	tcase_add_test(tc, test_septhread_message_rx_log);
	tcase_add_test(tc, test_septhread_message_rx_mq_open_failure);
	tcase_add_test(tc, test_septhread_message_rx_mq_setattr_failure1);
	tcase_add_test(tc, test_septhread_message_rx_mq_setattr_failure2);
	tcase_add_test(tc, test_septhread_message_rx_mq_receive_failure);
	tcase_add_test(tc, test_septhread_message_rx_bad_message);
	tcase_add_test(tc, test_septhread_message_rx_no_missed_message);
	tcase_add_test(tc, test_septhread_message_rx_missed_message);
	tcase_add_test(tc, test_septhread_message_rx_missed_message_wo_cb);

	suite_add_tcase(s, tc);

	return s;
}
