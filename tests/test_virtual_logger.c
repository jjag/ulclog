/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <check.h>
#include <stdbool.h>

#include "predictable_time.h"

static void setup(void);

struct Suite *get_suite(void);

#include "ulclog/ulclog_logger.h"

#include <stdlib.h>

static int ulclog_test_vctor(
	struct ulclog_logger *,
	va_list);
static void ulclog_test_dtor(
	struct ulclog_logger *);
static ssize_t ulclog_test_dolog(
	struct ulclog_logger *logger,
	int priority,
	const char *message,
	ssize_t nbchar);

static struct LoggerClass TestLogger = {
	.size = sizeof(struct ulclog_logger),
	.vctor = ulclog_test_vctor,
	.dtor  = ulclog_test_dtor,
	.dolog = ulclog_test_dolog,
};

static bool ulclog_test_vctor_called;
static int ulclog_test_vctor_return_value;
int ulclog_test_vctor(
	struct ulclog_logger *logger __attribute__((unused)),
	va_list pa __attribute__((unused)))
{
	ulclog_test_vctor_called = true;

	return ulclog_test_vctor_return_value;
}

static bool ulclog_test_dtor_called;
void ulclog_test_dtor(
	struct ulclog_logger *ulclog_logger __attribute__((unused)))
{
	ulclog_test_dtor_called = true;
}


struct ulclog_logger *ulclog_test_dolog_logger;
static int ulclog_test_dolog_priority;
static char ulclog_test_dolog_message[LOG_BUFFERS_SIZE];

static ssize_t ulclog_test_dolog(
	struct ulclog_logger *logger,
	int priority,
	const char *message,
	ssize_t nbchar __attribute__((unused)))
{
	ulclog_test_dolog_logger = logger;
	ulclog_test_dolog_priority = priority;

	strncpy(ulclog_test_dolog_message, message,
		sizeof(ulclog_test_dolog_message) - 1);

	return (signed)strlen(message);
}

static void setup(void)
{
	ulclog_test_vctor_called = false;
	ulclog_test_vctor_return_value = 0;

	ulclog_test_dtor_called = false;

	ulclog_test_dolog_logger = NULL;
	ulclog_test_dolog_priority = 0;
	ulclog_test_dolog_message[0] = '\0';
}


START_TEST(test_logger_new_delete)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&TestLogger);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_int_eq(ulclog_test_vctor_called, true);

	ulclog_logger_delete(logger);

	ck_assert_int_eq(ulclog_test_dtor_called, true);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#define LOG_METHOD		ulclog_logger_log
#define LOG_CONTEXT(level)	LOG_PARAMS(logger, level)
#define LOG_MODULE		"test_logger"
#define PRE_FORMAT		"Tadaaam!"
#define POST_FORMAT		""
#define LOG_PRIORITY_MASK	LOG_UPTO(LOG_DEBUG)

#define LOG(level, format, ...)				\
	LOG_METHOD(LOG_CONTEXT(level),			\
		   PRE_FORMAT format POST_FORMAT,	\
		   ##__VA_ARGS__)

#include "ulclog/ulclog.h"

START_TEST(test_logger_log)
{
	struct ulclog_logger *logger;
	ssize_t nbchar;

	logger = ulclog_logger_new(&TestLogger);

	nbchar = LWARNING("warning");

	ck_assert_int_eq(nbchar, 15);
	ck_assert_ptr_eq(ulclog_test_dolog_logger, logger);
	ck_assert_int_eq(ulclog_test_dolog_priority, LOG_WARNING);
	ck_assert_str_eq(ulclog_test_dolog_message, "Tadaaam!warning");

	ulclog_logger_delete(logger);
}
END_TEST

START_TEST(test_logger_prio_filtering)
{
	struct ulclog_logger *logger;
	int previous_prio_mask;

	logger = ulclog_logger_new(&TestLogger);

	LWARNING("warning");

	ck_assert_str_eq(ulclog_test_dolog_message, "Tadaaam!warning");

	ulclog_test_dolog_message[0] = '\0';

	/* everything but LOG_WARNING */
	previous_prio_mask = ulclog_logger_set_prio_mask(
		logger, LOG_UPTO(LOG_DEBUG) & ~LOG_MASK(LOG_WARNING));
	LWARNING("warning");

	ck_assert_str_eq(ulclog_test_dolog_message, "");
	ck_assert_int_eq(previous_prio_mask, LOG_UPTO(LOG_INFO));

	previous_prio_mask = ulclog_logger_set_prio_mask(
		logger, LOG_UPTO(LOG_ERR));

	ck_assert_int_eq(previous_prio_mask,
			 LOG_UPTO(LOG_DEBUG) & ~LOG_MASK(LOG_WARNING));

	ulclog_logger_delete(logger);
}
END_TEST

START_TEST(test_logger_prio_external_mask)
{
	struct ulclog_logger *logger;
	int extern_prio_mask;

	logger = ulclog_logger_new(&TestLogger);

	ulclog_logger_set_external_mask(logger, &extern_prio_mask);

	/* everything but LOG_ERR */
	extern_prio_mask = LOG_UPTO(LOG_DEBUG) & ~LOG_MASK(LOG_ERR);

	LERROR("error");
	ck_assert_str_eq(ulclog_test_dolog_message, "");

	ulclog_logger_set_external_mask(logger, NULL);

	LERROR("error");
	ck_assert_str_eq(ulclog_test_dolog_message, "");

	ulclog_logger_set_prio_mask(logger, LOG_UPTO(LOG_ERR));

	LERROR("error");
	ck_assert_str_eq(ulclog_test_dolog_message, "Tadaaam!error");

	ulclog_logger_delete(logger);
}
END_TEST

struct test_decorator_context {
	const char *preamble;
	const char *afterword;
};

struct test_decorator_context test_decorator_context = {
	.preamble  = "-->",
	.afterword = "<--",
};

static ssize_t
test_decorator_before(
	void *context,
	struct ulclog_logger *ulclog_logger,
	int priority,
	char *message,
	ssize_t message_buffer_size,
	ssize_t offset);

ssize_t
test_decorator_before(
	void *context,
	struct ulclog_logger *ulclog_logger __attribute__((unused)),
	int priority __attribute__((unused)),
	char *message,
	ssize_t message_buffer_size,
	ssize_t offset)
{
	struct test_decorator_context *test_context = context;

	strncpy(message, test_context->preamble,
		(unsigned)(message_buffer_size - offset));

	return (signed)strlen(test_context->preamble);
}

static ssize_t
test_decorator_after(
	void *context,
	struct ulclog_logger *ulclog_logger,
	int priority,
	char *message,
	ssize_t message_buffer_size,
	ssize_t offset);
ssize_t

test_decorator_after(
	void *context,
	struct ulclog_logger *ulclog_logger __attribute__((unused)),
	int priority __attribute__((unused)),
	char *message,
	ssize_t message_buffer_size,
	ssize_t offset)
{
	struct test_decorator_context *test_context = context;

	strncpy(message + offset, test_context->afterword,
		(unsigned)(message_buffer_size - offset));

	return (signed)strlen(test_context->afterword);
}

struct ulclog_logger_decorator test_decorator = {
	.context = &test_decorator_context,
	.before = test_decorator_before,
	.after = test_decorator_after,
};

struct ulclog_logger_decorator test_decorator_no_after = {
	.context = &test_decorator_context,
	.before = test_decorator_before,
	.after = NULL,
};

struct ulclog_logger_decorator test_decorator_no_before = {
	.context = &test_decorator_context,
	.before = NULL,
	.after = test_decorator_after,
};

START_TEST(test_logger_decorator)
{
	struct ulclog_logger *logger;
	ssize_t nbchar;

	logger = ulclog_logger_new(&TestLogger);

	ulclog_logger_set_decorator(logger, &test_decorator);

	nbchar = LERROR("error");

	ck_assert_str_eq(ulclog_test_dolog_message, "-->Tadaaam!error<--");
	ck_assert_int_eq(nbchar, 19);

	ulclog_logger_set_decorator(logger, &test_decorator_no_after);

	nbchar = LERROR("error");

	ck_assert_str_eq(ulclog_test_dolog_message, "-->Tadaaam!error");
	ck_assert_int_eq(nbchar, 16);

	ulclog_logger_set_decorator(logger, &test_decorator_no_before);

	nbchar = LERROR("error");

	ck_assert_str_eq(ulclog_test_dolog_message, "Tadaaam!error<--");
	ck_assert_int_eq(nbchar, 16);

	ulclog_logger_set_decorator(logger, NULL);

	nbchar = LERROR("error");

	ck_assert_str_eq(ulclog_test_dolog_message, "Tadaaam!error");
	ck_assert_int_eq(nbchar, 13);

	ulclog_logger_delete(logger);
}
END_TEST

struct Suite *get_suite(void)
{
	struct Suite *s;
	struct TCase *tc;

	s = suite_create("Generic logger");

	/* Core test case */
	tc = tcase_create("Generic logger");

	tcase_add_checked_fixture(tc, setup, NULL);

	tcase_add_test(tc, test_logger_new_delete);
	tcase_add_test(tc, test_logger_log);
	tcase_add_test(tc, test_logger_prio_filtering);
	tcase_add_test(tc, test_logger_prio_external_mask);
	tcase_add_test(tc, test_logger_decorator);

	suite_add_tcase(s, tc);

	return s;
}
