#!/usr/bin/env bash

# Copyright 2016 Jérôme Arbez-Gindre

# This file is part of UlClog.

# UlClog is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# UlClog is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with UlClog.  If not, see
# <http://www.gnu.org/licenses/>.

./test_build_time_log_level 2> ./test_build_time_log_level.output

expected=$(cat<<EOF
--- default priorities up to LOG_INFO ---
1000.999999:emerg  :0:module_name:main      :EMERGENCY octal 123
1001.999998:alert  :1:module_name:main      :ALERT     integer 1
1002.999997:crit   :2:module_name:main      :CRITICAL  float 1.100000
1003.999996:err    :3:module_name:main      :ERROR     string pouet
1004.999995:warning:4:module_name:main      :WARNING   only that
1005.999994:notice :5:module_name:main      :NOTICE    pointer 0x12345678
1006.999993:info   :6:module_name:main      :INFO      hexa abcdef01
--- priorities up to LOG_DEBUG ---
1007.999992:emerg  :0:module_name:main      :EMERGENCY octal 123
1008.999991:alert  :1:module_name:main      :ALERT     integer 1
1009.999990:crit   :2:module_name:main      :CRITICAL  float 1.100000
1010.999989:err    :3:module_name:main      :ERROR     string pouet
1011.999988:warning:4:module_name:main      :WARNING   only that
1012.999987:notice :5:module_name:main      :NOTICE    pointer 0x12345678
1013.999986:info   :6:module_name:main      :INFO      hexa abcdef01
1014.999985:debug  :7:module_name:main      :filename   :0:
--- priorities up to LOG_INFO ---
1015.999984:emerg  :0:module_name:main      :EMERGENCY octal 123
1016.999983:alert  :1:module_name:main      :ALERT     integer 1
1017.999982:crit   :2:module_name:main      :CRITICAL  float 1.100000
1018.999981:err    :3:module_name:main      :ERROR     string pouet
1019.999980:warning:4:module_name:main      :WARNING   only that
1020.999979:notice :5:module_name:main      :NOTICE    pointer 0x12345678
1021.999978:info   :6:module_name:main      :INFO      hexa abcdef01
--- priorities up to LOG_NOTICE ---
1022.999977:emerg  :0:module_name:main      :EMERGENCY octal 123
1023.999976:alert  :1:module_name:main      :ALERT     integer 1
1024.999975:crit   :2:module_name:main      :CRITICAL  float 1.100000
1025.999974:err    :3:module_name:main      :ERROR     string pouet
1026.999973:warning:4:module_name:main      :WARNING   only that
1027.999972:notice :5:module_name:main      :NOTICE    pointer 0x12345678
--- priorities up to LOG_WARNING ---
1028.999971:emerg  :0:module_name:main      :EMERGENCY octal 123
1029.999970:alert  :1:module_name:main      :ALERT     integer 1
1030.999969:crit   :2:module_name:main      :CRITICAL  float 1.100000
1031.999968:err    :3:module_name:main      :ERROR     string pouet
1032.999967:warning:4:module_name:main      :WARNING   only that
--- priorities up to LOG_ERR ---
1033.999966:emerg  :0:module_name:main      :EMERGENCY octal 123
1034.999965:alert  :1:module_name:main      :ALERT     integer 1
1035.999964:crit   :2:module_name:main      :CRITICAL  float 1.100000
1036.999963:err    :3:module_name:main      :ERROR     string pouet
--- priorities up to LOG_CRIT ---
1037.999962:emerg  :0:module_name:main      :EMERGENCY octal 123
1038.999961:alert  :1:module_name:main      :ALERT     integer 1
1039.999960:crit   :2:module_name:main      :CRITICAL  float 1.100000
--- priorities up to LOG_ALERT ---
1040.999959:emerg  :0:module_name:main      :EMERGENCY octal 123
1041.999958:alert  :1:module_name:main      :ALERT     integer 1
--- priorities up to LOG_EMERG ---
1042.999957:emerg  :0:module_name:main      :EMERGENCY octal 123
--- No logs at all ---
--- only ERROR and NOTICE---
1043.999956:err    :3:module_name:main      :ERROR     string pouet
1044.999955:notice :5:module_name:main      :NOTICE    pointer 0x12345678
--- Everything but WARNING ---
1045.999954:emerg  :0:module_name:main      :EMERGENCY octal 123
1046.999953:alert  :1:module_name:main      :ALERT     integer 1
1047.999952:crit   :2:module_name:main      :CRITICAL  float 1.100000
1048.999951:err    :3:module_name:main      :ERROR     string pouet
1049.999950:notice :5:module_name:main      :NOTICE    pointer 0x12345678
1050.999949:info   :6:module_name:main      :INFO      hexa abcdef01
1051.999948:debug  :7:module_name:main      :filename   :0:
EOF
)
echo "$expected" > ./test_build_time_log_level.expected_output

diff -u ./test_build_time_log_level.expected_output ./test_build_time_log_level.output

exit $?
