/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdarg.h>

#include <check.h>

#include "predictable_time.h"

static void setup(void);

struct Suite *get_suite(void);

#include <sys/socket.h>
#include <netinet/in.h>

/* socket stub */
static int socket_stub_domain;
static int socket_stub_type;
static int socket_stub_protocol;
int socket(
	int domain,
	int type,
	int protocol)
{
	socket_stub_domain = domain;
	socket_stub_type = type;
	socket_stub_protocol = protocol;

	return 123;
}

/* close stub */
int close_stub_fildes;
int close(int fildes)
{
	close_stub_fildes = fildes;
	return 0;
}

/* sendto stub */
static int sendto_stub_socket;
static char sendto_stub_message[LOG_BUFFERS_SIZE];
static size_t sendto_stub_length;
static int sendto_stub_flags;
static const struct sockaddr_in *sendto_stub_dest_addr;
static socklen_t sendto_stub_dest_len;
static ssize_t sendto_stub_return_value;
ssize_t sendto(
	int socket,
	const void *message,
	size_t length,
	int flags,
	const struct sockaddr *dest_addr,
	socklen_t dest_len)
{
	sendto_stub_socket = socket;
	memcpy(sendto_stub_message, message, length);
	sendto_stub_length = length;
	sendto_stub_flags = flags;
	sendto_stub_dest_addr = (const struct sockaddr_in *) dest_addr;
	sendto_stub_dest_len = dest_len;

	return sendto_stub_return_value;
}

/* setsockopt stub */
static int setsockopt_stub_sockfd;
static int setsockopt_stub_level;
static int setsockopt_stub_optname;
static const void *setsockopt_stub_optval;
static socklen_t setsockopt_stub_optlen;
static int setsockopt_stub_return_value;
int setsockopt(
	int sockfd,
	int level,
	int optname,
	const void *optval,
	socklen_t optlen)
{
	setsockopt_stub_sockfd  = sockfd;
	setsockopt_stub_level   = level;
	setsockopt_stub_optname = optname;
	setsockopt_stub_optval  = optval;
	setsockopt_stub_optlen  = optlen;

	return setsockopt_stub_return_value;
}

/* getsockopt stub */
static int getsockopt_stub_sockfd;
static int getsockopt_stub_level;
static int getsockopt_stub_optname;
static const void *getsockopt_stub_optval;
static socklen_t *getsockopt_stub_optlen;
static int getsockopt_stub_return_value;
int getsockopt(
	int sockfd,
	int level,
	int optname,
	void *__restrict optval,
	socklen_t *__restrict optlen)
{
	getsockopt_stub_sockfd  = sockfd;
	getsockopt_stub_level   = level;
	getsockopt_stub_optname = optname;
	getsockopt_stub_optval  = optval;
	getsockopt_stub_optlen  = optlen;

	return getsockopt_stub_return_value;
}

void setup(void)
{
	socket_stub_domain = -1;
	socket_stub_type = -1;
	socket_stub_protocol = -1;

	close_stub_fildes = 0;

	sendto_stub_socket = 0;
	sendto_stub_message[0] = '\0';
	sendto_stub_length = 0;
	sendto_stub_flags = -1;
	sendto_stub_dest_addr = NULL;
	sendto_stub_dest_len = 0;
	sendto_stub_return_value = 0;

	setsockopt_stub_sockfd = 0;
	setsockopt_stub_level = 0;
	setsockopt_stub_optname = 0;
	setsockopt_stub_optval = NULL;
	setsockopt_stub_optlen = 0;
	setsockopt_stub_return_value = 0;

	getsockopt_stub_sockfd = 0;
	getsockopt_stub_level = 0;
	getsockopt_stub_optname = 0;
	getsockopt_stub_optval = NULL;
	getsockopt_stub_optlen = NULL;
	getsockopt_stub_return_value = 0;
}

#include "ulclog/ulclog_logger_udp.h"

START_TEST(test_udp_init_clean)
{
	struct ulclog_udp_logger logger;

	ulclog_udp_init(&logger, "127.0.0.1", 12345);

	ck_assert_int_eq(socket_stub_domain, AF_INET);
	ck_assert_int_eq(socket_stub_type, SOCK_DGRAM);
	ck_assert_int_eq(socket_stub_protocol, IPPROTO_UDP);

	ulclog_udp_clean(&logger);

	ck_assert_int_eq(close_stub_fildes, 123);
}
END_TEST

START_TEST(test_udp_init_failure)
{
	struct ulclog_udp_logger logger;
	int init_res;

	init_res = ulclog_udp_init(&logger, "999.0.0.1", 12345);

	ck_assert_int_eq(init_res, -1);
}
END_TEST

START_TEST(test_udp_new_delete)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&UdpLogger, "127.0.0.1", 12345);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_int_eq(socket_stub_domain, AF_INET);
	ck_assert_int_eq(socket_stub_type, SOCK_DGRAM);

	ck_assert_int_eq(socket_stub_protocol, IPPROTO_UDP);

	ulclog_logger_delete(logger);

	ck_assert_int_eq(close_stub_fildes, 123);
}
END_TEST

START_TEST(test_udp_new_failure_bad_ip)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&UdpLogger, "888.0.0.1", 12345);

	ck_assert_ptr_eq(logger, NULL);
}
END_TEST

START_TEST(test_udp_new_failure_high_port)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&UdpLogger, "127.0.0.1", 123456);

	ck_assert_ptr_eq(logger, NULL);
}
END_TEST

START_TEST(test_udp_new_failure_low_port)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&UdpLogger, "127.0.0.1", -123);

	ck_assert_ptr_eq(logger, NULL);
}
END_TEST

START_TEST(test_udp_new_failure_0_port)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&UdpLogger, "127.0.0.1", 0);

	ck_assert_ptr_eq(logger, NULL);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#define LOG_METHOD		ulclog_logger_log
#define LOG_CONTEXT(level)	LOG_PARAMS(logger, level)
#define LOG_MODULE		"test_logger"
#define PRE_FORMAT		"Tadaaam!"
#define POST_FORMAT		""

#define LOG(level, format, ...)				\
	LOG_METHOD(LOG_CONTEXT(level),			\
		   PRE_FORMAT format POST_FORMAT,	\
		   ##__VA_ARGS__)

#include "ulclog/ulclog.h"

START_TEST(test_udp_log)
{
	struct ulclog_logger *logger;

	const char *dest_ip = "127.0.0.1";
	unsigned short dest_port = 12345;
	ssize_t nbchar;

	logger = ulclog_logger_new(&UdpLogger, dest_ip, dest_port);

	sendto_stub_return_value = 13;

	nbchar = LINFO("INFO");

	ck_assert_int_eq(nbchar, 12);
	ck_assert_int_eq(sendto_stub_socket, 123);
	ck_assert_str_eq(sendto_stub_message, "Tadaaam!INFO");
	ck_assert_int_eq((intmax_t)sendto_stub_length, 13);
	ck_assert_int_eq(sendto_stub_flags, 0);
	ck_assert_int_eq(sendto_stub_dest_addr->sin_family, AF_INET);
	ck_assert_int_eq(sendto_stub_dest_addr->sin_addr.s_addr, 0x0100007F);
	ck_assert_int_eq(sendto_stub_dest_addr->sin_port, 0x3930);
	ck_assert_int_eq(sendto_stub_dest_len, sizeof(*sendto_stub_dest_addr));

	ulclog_logger_delete(logger);
}
END_TEST

START_TEST(test_udp_log_with_init_clean)
{
	struct ulclog_udp_logger udp_logger;
	struct ulclog_logger *logger = (struct ulclog_logger *)&udp_logger;

	const char *dest_ip = "127.0.0.1";
	unsigned short dest_port = 12345;
	ssize_t nbchar;

	ulclog_udp_init(&udp_logger, dest_ip, dest_port);

	sendto_stub_return_value = 13;

	nbchar = LINFO("INFO");

	ck_assert_int_eq(nbchar, 12);
	ck_assert_int_eq(sendto_stub_socket, 123);
	ck_assert_str_eq(sendto_stub_message, "Tadaaam!INFO");
	ck_assert_int_eq((intmax_t)sendto_stub_length, 13);
	ck_assert_int_eq(sendto_stub_flags, 0);
	ck_assert_int_eq(sendto_stub_dest_addr->sin_family, AF_INET);
	ck_assert_int_eq(sendto_stub_dest_addr->sin_addr.s_addr, 0x0100007F);
	ck_assert_int_eq(sendto_stub_dest_addr->sin_port, 0x3930);
	ck_assert_int_eq(sendto_stub_dest_len, sizeof(*sendto_stub_dest_addr));

	ulclog_udp_clean(&udp_logger);
}
END_TEST

START_TEST(test_udp_failed_log)
{
	struct ulclog_logger *logger;

	const char *dest_ip = "127.0.0.1";
	unsigned short dest_port = 12345;
	ssize_t nbchar;

	logger = ulclog_logger_new(&UdpLogger, dest_ip, dest_port);

	sendto_stub_return_value = -1;

	nbchar = LINFO("INFO");

	ck_assert_int_eq(nbchar, -1);

	ulclog_logger_delete(logger);
}
END_TEST

START_TEST(test_udp_setsockopt)
{
	struct ulclog_logger *logger;

	const char *dest_ip = "127.0.0.1";
	unsigned short dest_port = 12345;
	int dontroute = 1;
	int return_value;

	logger = ulclog_logger_new(&UdpLogger, dest_ip, dest_port);

	setsockopt_stub_return_value = -1;

	return_value = ulclog_udp_setsockopt((struct ulclog_udp_logger *)logger,
					    SOL_SOCKET,
					    SO_DONTROUTE,
					    &dontroute,
					    sizeof(dontroute));

	ck_assert_int_eq(return_value, setsockopt_stub_return_value);
	ck_assert_int_eq(setsockopt_stub_sockfd, 123);
	ck_assert_int_eq(setsockopt_stub_level, SOL_SOCKET);
	ck_assert_int_eq(setsockopt_stub_optname, SO_DONTROUTE);
	ck_assert_ptr_eq(setsockopt_stub_optval, &dontroute);
	ck_assert_int_eq(setsockopt_stub_optlen, sizeof(dontroute));

	ulclog_logger_delete(logger);
}
END_TEST

START_TEST(test_udp_getsockopt)
{
	struct ulclog_logger *logger;

	const char *dest_ip = "127.0.0.1";
	unsigned short dest_port = 12345;
	int dontroute;
	socklen_t dontroute_len;
	int return_value;

	logger = ulclog_logger_new(&UdpLogger, dest_ip, dest_port);

	getsockopt_stub_return_value = -1;

	return_value = ulclog_udp_getsockopt((struct ulclog_udp_logger *)logger,
					    SOL_SOCKET,
					    SO_DONTROUTE,
					    &dontroute,
					    &dontroute_len);

	ck_assert_int_eq(return_value, getsockopt_stub_return_value);
	ck_assert_int_eq(getsockopt_stub_sockfd, 123);
	ck_assert_int_eq(getsockopt_stub_level, SOL_SOCKET);
	ck_assert_int_eq(getsockopt_stub_optname, SO_DONTROUTE);
	ck_assert_ptr_eq(getsockopt_stub_optval, &dontroute);
	ck_assert_ptr_eq(getsockopt_stub_optlen, &dontroute_len);

	ulclog_logger_delete(logger);
}
END_TEST

struct Suite *get_suite(void)
{
	struct Suite *s;
	struct TCase *tc;

	s = suite_create("Udp back-end");

	/* Core test case */
	tc = tcase_create("Udp back-end");

	tcase_add_checked_fixture(tc, setup, NULL);

	tcase_add_test(tc, test_udp_init_clean);
	tcase_add_test(tc, test_udp_init_failure);
	tcase_add_test(tc, test_udp_new_delete);
	tcase_add_test(tc, test_udp_new_failure_bad_ip);
	tcase_add_test(tc, test_udp_new_failure_high_port);
	tcase_add_test(tc, test_udp_new_failure_low_port);
	tcase_add_test(tc, test_udp_new_failure_0_port);
	tcase_add_test(tc, test_udp_log);
	tcase_add_test(tc, test_udp_log_with_init_clean);
	tcase_add_test(tc, test_udp_failed_log);
	tcase_add_test(tc, test_udp_setsockopt);
	tcase_add_test(tc, test_udp_getsockopt);

	suite_add_tcase(s, tc);

	return s;
}

