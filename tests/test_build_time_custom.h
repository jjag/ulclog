/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

/* Redefine log method/context to ease tests */
#define LOG_METHOD		snprintf
#define LOG_CONTEXT(level)	LOG_PARAMS(log_string, sizeof(log_string))
/* Define log level to log everything */
#define LOG_PRIORITY_MASK	LOG_UPTO(LOG_DEBUG)
/* Define file/line to ease tests */
#define FILE_LINE_VALUE		LOG_PARAMS("file", 0)
/* Define module */
#define LOG_MODULE		"test"
