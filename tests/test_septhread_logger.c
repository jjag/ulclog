/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>

#include <check.h>

#include "predictable_time.h"

static void setup(void);
static void teardown(void);

struct Suite *get_suite(void);


#include <semaphore.h>
#include "ulclog/ulclog_logger.h"

struct ulclog_test_logger {
	struct ulclog_logger ulclog_logger;
	int priority;
	char message[LOG_BUFFERS_SIZE];
	ssize_t dolog_return_value;
	sem_t msg_ready_sem;
	sem_t get_next_sem;
};

static int ulclog_test_vctor(
	struct ulclog_logger *, va_list);
static void ulclog_test_dtor(
	struct ulclog_logger *);
static ssize_t ulclog_test_dolog(
	struct ulclog_logger *,
	int,
	const char *,
	ssize_t);

static struct LoggerClass TestLogger = {
	.size = sizeof(struct ulclog_test_logger),
	.vctor = ulclog_test_vctor,
	.dtor  = ulclog_test_dtor,
	.dolog = ulclog_test_dolog,
};

int ulclog_test_vctor(
	struct ulclog_logger *ulclog_logger,
	va_list pa __attribute__((unused)))
{
	struct ulclog_test_logger *ulclog_test_logger =
		(struct ulclog_test_logger *)ulclog_logger;

	ulclog_test_logger->priority = -1;
	ulclog_test_logger->message[0] = '\0';
	ulclog_test_logger->dolog_return_value = 0;

	sem_init(&ulclog_test_logger->msg_ready_sem, 0, 0);
	sem_init(&ulclog_test_logger->get_next_sem, 0, 0);

	return 0;
}

void ulclog_test_dtor(
	struct ulclog_logger *ulclog_logger)
{
	struct ulclog_test_logger *ulclog_test_logger =
		(struct ulclog_test_logger *)ulclog_logger;

	sem_destroy(&ulclog_test_logger->msg_ready_sem);
	sem_destroy(&ulclog_test_logger->get_next_sem);
}

static ssize_t ulclog_test_dolog(
	struct ulclog_logger *ulclog_logger,
	int priority,
	const char *message,
	ssize_t nbchar __attribute__((unused)))
{
	struct ulclog_test_logger *ulclog_test_logger =
		(struct ulclog_test_logger *)ulclog_logger;
	ulclog_test_logger->priority = priority;
	strncpy(ulclog_test_logger->message, message,
		sizeof(ulclog_test_logger->message) - 1);

	sem_post(&ulclog_test_logger->msg_ready_sem);
	sem_wait(&ulclog_test_logger->get_next_sem);

	return ulclog_test_logger->dolog_return_value;
}

static struct ulclog_logger *test_logger;
static struct ulclog_test_logger *ulclog_test_logger;
void setup(void)
{
	test_logger = ulclog_logger_new(&TestLogger);
	ulclog_test_logger = (struct ulclog_test_logger *)test_logger;
}

void teardown(void)
{
	ulclog_test_logger = NULL;
	ulclog_logger_delete(test_logger);
}

#include "ulclog/ulclog_logger_septhread.h"

START_TEST(test_septhread_init_clean)
{
	struct ulclog_septhread_logger logger;
	int init_return_value;

	init_return_value = ulclog_septhread_init(&logger,
						 test_logger,
						 NULL,
						 10,
						 NULL);

	ck_assert_int_eq(init_return_value, 0);

	ulclog_septhread_clean(&logger);
}
END_TEST

START_TEST(test_septhread_new_delete)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&SepThreadLogger,
				  test_logger,
				  NULL,
				  10,
				  NULL);

	ck_assert_ptr_ne(logger, NULL);

	ulclog_logger_delete(logger);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#define LOG_METHOD		ulclog_logger_log
#define LOG_CONTEXT(level)	LOG_PARAMS(logger, level)
#define LOG_MODULE		"test_logger"
#define PRE_FORMAT		"Tadaaam!"
#define POST_FORMAT		""

#define LOG(level, format, ...)				\
	LOG_METHOD(LOG_CONTEXT(level),			\
		   PRE_FORMAT format POST_FORMAT,	\
		   ##__VA_ARGS__)

#include "ulclog/ulclog.h"

START_TEST(test_septhread_log)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&SepThreadLogger,
				  test_logger,
				  NULL,
				  10,
				  NULL);

	ck_assert_ptr_ne(logger, NULL);

	LWARNING("WARNING");

	sem_wait(&ulclog_test_logger->msg_ready_sem);

	ck_assert_str_eq(ulclog_test_logger->message, "Tadaaam!WARNING");

	sem_post(&ulclog_test_logger->get_next_sem);

	ulclog_logger_delete(logger);
}
END_TEST

static void missed_msg_cb(
	struct ulclog_logger *ulclog_logger,
	unsigned int nmissed);

START_TEST(test_septhread_log_missed_message)
{
	struct ulclog_logger *logger;
	ssize_t nbchar;

	logger = ulclog_logger_new(&SepThreadLogger,
				  test_logger,
				  NULL,
				  1, /* only one message */
				  missed_msg_cb);

	ck_assert_ptr_ne(logger, NULL);

	nbchar = LWARNING("WARNING 1"); /* this one is handled  */
	ck_assert_int_eq(nbchar, 17);

	/* need to wait to be sure that first message is handled */
	sem_wait(&ulclog_test_logger->msg_ready_sem);

	nbchar = LWARNING("WARNING 2"); /* this-one is stored in the queue */
	ck_assert_int_eq(nbchar, 17);

	for (int i = 0 ; i < 78 ; i++) {
		nbchar = LWARNING("WARNING 3"); /* this-one should be
						 * lost */
		ck_assert_int_eq(nbchar, 0);
	}

	ck_assert_str_eq(ulclog_test_logger->message, "Tadaaam!WARNING 1");

	sem_post(&ulclog_test_logger->get_next_sem);
	sem_wait(&ulclog_test_logger->msg_ready_sem);

	ck_assert_str_eq(ulclog_test_logger->message, "Tadaaam!WARNING 2");

	sem_post(&ulclog_test_logger->get_next_sem);

	nbchar = LWARNING("WARNING 4"); /* this-one is should be handled */
	ck_assert_int_eq(nbchar, 17);

	sem_wait(&ulclog_test_logger->msg_ready_sem);

	ck_assert_str_eq(ulclog_test_logger->message,
			 "Tadaaam!Missed 78 messages");

	sem_post(&ulclog_test_logger->get_next_sem);

	sem_wait(&ulclog_test_logger->msg_ready_sem);

	ck_assert_str_eq(ulclog_test_logger->message, "Tadaaam!WARNING 4");

	sem_post(&ulclog_test_logger->get_next_sem);

	ulclog_logger_delete(logger);
}
END_TEST

#undef LOG_CONTEXT
#define LOG_CONTEXT(level)	LOG_PARAMS(ulclog_logger, level)

void missed_msg_cb(
	struct ulclog_logger *ulclog_logger,
	unsigned int nmissed)
{
	LWARNING("Missed %d messages", nmissed);
}

struct Suite *get_suite(void)
{
	struct Suite *s;
	struct TCase *tc;

	s = suite_create("Separate thread back-end");

	/* Core test case */
	tc = tcase_create("Separate thread back-end");

	tcase_add_checked_fixture(tc, setup, teardown);

	tcase_add_test(tc, test_septhread_init_clean);
	tcase_add_test(tc, test_septhread_new_delete);
	tcase_add_test(tc, test_septhread_log);
	tcase_add_test(tc, test_septhread_log_missed_message);

	suite_add_tcase(s, tc);

	return s;
}
