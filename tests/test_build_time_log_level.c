/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdio.h>

#include "predictable_time.h"

#define ALL_SORT_OF_LOGS()						\
	do {								\
		LEMERGENCY("EMERGENCY octal %03o", 0123);		\
		LALERT("ALERT     integer %d", 1);			\
		LCRITICAL("CRITICAL  float %f", 1.1);			\
		LERROR("ERROR     string %s", "pouet");			\
		LWARNING("WARNING   only that");			\
		LNOTICE("NOTICE    pointer %p", (void *)0x12345678);	\
		LINFO("INFO      hexa %08x", 0xabcdef01);		\
		LDEBUG();						\
	} while (0)

int main(void)
{
#include "ulclog/ulclog_undef_all.h"
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- default priorities up to LOG_INFO ---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_DEBUG)
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- priorities up to LOG_DEBUG ---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_INFO)
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- priorities up to LOG_INFO ---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_NOTICE)
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- priorities up to LOG_NOTICE ---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_WARNING)
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- priorities up to LOG_WARNING ---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_ERR)
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- priorities up to LOG_ERR ---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_CRIT)
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- priorities up to LOG_CRIT ---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_ALERT)
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- priorities up to LOG_ALERT ---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK LOG_UPTO(LOG_EMERG)
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- priorities up to LOG_EMERG ---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK 0
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- No logs at all ---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK (LOG_MASK(LOG_ERR) | LOG_MASK(LOG_NOTICE))
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- only ERROR and NOTICE---\n");
	ALL_SORT_OF_LOGS();

#include "ulclog/ulclog_undef_all.h"
#define LOG_PRIORITY_MASK (LOG_UPTO(LOG_DEBUG) & (~LOG_MASK(LOG_WARNING)))
#define FILE_LINE_VALUE	"filename", 0
#define LOG_MODULE	 "module_name"
#include "ulclog/ulclog.h"
	fprintf(stderr, "--- Everything but WARNING ---\n");
	ALL_SORT_OF_LOGS();

	return EXIT_SUCCESS;
}
