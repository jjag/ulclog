/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <syslog.h>
#include <check.h>
#include <stdio.h>
#include <stdarg.h>

static void setup(void);

struct Suite *get_suite(void);

/* syslog stub */
static int syslog_stub_priority;
static char syslog_stub_message[LOG_BUFFERS_SIZE];
void syslog(int priority, const char *format, ...)
{
	va_list pa;

	syslog_stub_priority = priority;

	va_start(pa, format);
	vsnprintf(syslog_stub_message, sizeof(syslog_stub_message), format, pa);
	va_end(pa);
}

void setup(void)
{
	syslog_stub_priority = -1;
	syslog_stub_message[0] = '\0';
}

#include "ulclog/ulclog_undef_all.h"

#define PRE_FORMAT		"Tadaaam!"
#define POST_FORMAT		""
#define LOG_MODULE		"tests"

#define LOG(level, format, ...)					\
	({							\
		LOG_METHOD(LOG_CONTEXT(level),			\
			   PRE_FORMAT format POST_FORMAT,	\
			   ##__VA_ARGS__);			\
		0;						\
	})

#include "ulclog/ulclog_syslog.h"

START_TEST(test_syslog_buildtime_log)
{
	LINFO("INFO");

	ck_assert_int_eq(syslog_stub_priority, LOG_INFO);
	ck_assert_str_eq(syslog_stub_message, "Tadaaam!INFO");
}
END_TEST

struct Suite *get_suite(void)
{
	struct Suite *s;
	struct TCase *tc;

	s = suite_create("Syslog back-end");

	/* Core test case */
	tc = tcase_create("Syslog back-end");

	tcase_add_checked_fixture(tc, setup, NULL);

	tcase_add_test(tc, test_syslog_buildtime_log);

	suite_add_tcase(s, tc);

	return s;
}
