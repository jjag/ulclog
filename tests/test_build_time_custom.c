/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdarg.h>
#include <check.h>

#include "predictable_time.h"

char log_string[LOG_BUFFERS_SIZE] = "";


static void setup(void);

struct Suite *get_suite(void);

void setup(void)
{
	reset_gettimeofday_ref();
	log_string[0] = '\0';
}

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#include "ulclog/ulclog.h"

START_TEST(test_log_method)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "1000.999999:debug  :7:test      :test_log_method_fn:file       :0:DEBUG\n");

}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define SEP "#"
#include "ulclog/ulclog.h"

START_TEST(test_sep)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "1000.999999#debug  #7#test      #test_sep_fn#file       :0#DEBUG\n");

}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define FILE_LINE_SEP ">-<"
#include "ulclog/ulclog.h"

START_TEST(test_file_line_sep)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "1000.999999:debug  :7:test      :test_file_line_sep_fn:file       >-<0:DEBUG\n");

}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#undef LOG_MODULE
#define LOG_MODULE 17
#define MODULE_FORMAT "--%d--"
#include "ulclog/ulclog.h"

START_TEST(test_module)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "1000.999999:debug  :7:--17--:test_module_fn:file       :0:DEBUG\n");
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define MODULE_VALUE	"TOTO"
#include "ulclog/ulclog.h"

START_TEST(test_module_value)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "1000.999999:debug  :7:TOTO      :test_module_value_fn:file       :0:DEBUG\n");
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define SHOW_TIME 0
#include "ulclog/ulclog.h"

START_TEST(test_show_time)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "debug  :7:test      :test_show_time_fn:file       :0:DEBUG\n");
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define TIME_DECL char *pouet
#define TIME_EVAL				\
	do {					\
		pouet = "TIME";			\
	} while (0)
#define TIME_FORMAT	"%s"
#define TIME_VALUE      pouet
#include "ulclog/ulclog.h"

START_TEST(test_time)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "TIME:debug  :7:test      :test_time_fn:file       :0:DEBUG\n");
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define FUNC_FORMAT	"%d" SEP "%s"
#define FUNC_VALUE	LOG_PARAMS(14, "titi")
#include "ulclog/ulclog.h"

START_TEST(test_func)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "1000.999999:debug  :7:test      :14:titi:file       :0:DEBUG\n");
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define FILE_LINE_FORMAT	"%d" FILE_LINE_SEP "%-11s"
#undef FILE_LINE_VALUE
#define FILE_LINE_VALUE	        LOG_PARAMS(0, "file")
#include "ulclog/ulclog.h"

START_TEST(test_file_line)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "1000.999999:debug  :7:test      :test_file_line_fn:0:file       :DEBUG\n");
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define LEVEL_FORMAT	     "%s"
#define LEVEL_VALUE(level)	"something"
#include "ulclog/ulclog.h"

START_TEST(test_level_format)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "1000.999999:something:test      :test_level_format_fn:file       :0:DEBUG\n");
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define PRE_FORMAT		"%s"
#define PRE_VALUE(level)	""
#include "ulclog/ulclog.h"

START_TEST(test_pre)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "file       :0:DEBUG\n");
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define POST_FORMAT   "%s"
#define POST_VALUE    "---"
#include "ulclog/ulclog.h"

START_TEST(test_post)
{
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "1000.999999:debug  :7:test      :test_post_fn:file       :0:DEBUG---");
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define GUARD (level >= LOG_DEBUG)
#include "ulclog/ulclog.h"

START_TEST(test_guard)
{
	int level = 6;

	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string, "");
	level = 7;
	LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "1000.999999:debug  :7:test      :test_guard_fn:file       :0:DEBUG\n");
}
END_TEST


#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define PRE_FORMAT		"%s"
#define PRE_VALUE(level)	""
#include "ulclog/ulclog.h"

START_TEST(test_writen_nbchar)
{
	ssize_t nbchar;

	nbchar = LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "file       :0:DEBUG\n");
	ck_assert_int_eq(nbchar, 20);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define PRE_FORMAT		"%s"
#define PRE_VALUE(level)	""
#undef LOG_PRIORITY_MASK
#define LOG_PRIORITY_MASK       0
#include "ulclog/ulclog.h"

START_TEST(test_writen_nbchar_nolog)
{
	ssize_t nbchar;

	nbchar = LDEBUG("DEBUG");
	ck_assert_str_eq(log_string,
			 "");
	ck_assert_int_eq(nbchar, 0);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define PRE_FORMAT		"%s"
#define PRE_VALUE(level)	""
static int log_preamble_call_proof;
#define LOG_PREAMBLE(level)			\
	(log_preamble_call_proof += 1)
#include "ulclog/ulclog.h"

START_TEST(test_log_preamble)
{
	log_preamble_call_proof = 0;
	LDEBUG("DEBUG");
	ck_assert_int_eq(log_preamble_call_proof, 1);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define PRE_FORMAT		"%s"
#define PRE_VALUE(level)	""
#undef LOG_PRIORITY_MASK
#define LOG_PRIORITY_MASK       0
#define LOG_PREAMBLE(level)			\
	(log_preamble_call_proof += 1)
#include "ulclog/ulclog.h"

START_TEST(test_nolog_preamble)
{
	log_preamble_call_proof = 0;
	LDEBUG("DEBUG");
	ck_assert_int_eq(log_preamble_call_proof, 0);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define PRE_FORMAT		"%s"
#define PRE_VALUE(level)	""
static int log_afterword_call_proof;
#define LOG_AFTERWORD(level)			\
	(log_afterword_call_proof += 1)
#include "ulclog/ulclog.h"

START_TEST(test_log_afterword)
{
	log_afterword_call_proof = 0;
	LDEBUG("DEBUG");
	ck_assert_int_eq(log_afterword_call_proof, 1);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#include "test_build_time_custom.h"
#define PRE_FORMAT		"%s"
#define PRE_VALUE(level)	""
#undef LOG_PRIORITY_MASK
#define LOG_PRIORITY_MASK       0
#define LOG_AFTERWORD(level)			\
	(log_afterword_call_proof += 1)
#include "ulclog/ulclog.h"

START_TEST(test_nolog_afterword)
{
	log_afterword_call_proof = 0;
	LDEBUG("DEBUG");
	ck_assert_int_eq(log_afterword_call_proof, 0);
}
END_TEST

struct Suite *get_suite(void)
{
	struct Suite *s;
	struct TCase *tc;

	s = suite_create("Build time customization");

	/* Core test case */
	tc = tcase_create("Build time customization");

	tcase_add_checked_fixture(tc, setup, NULL);

	tcase_add_test(tc, test_log_method);
	tcase_add_test(tc, test_sep);
	tcase_add_test(tc, test_file_line_sep);
	tcase_add_test(tc, test_module);
	tcase_add_test(tc, test_module_value);
	tcase_add_test(tc, test_show_time);
	tcase_add_test(tc, test_time);
	tcase_add_test(tc, test_func);
	tcase_add_test(tc, test_file_line);
	tcase_add_test(tc, test_level_format);
	tcase_add_test(tc, test_pre);
	tcase_add_test(tc, test_post);
	tcase_add_test(tc, test_guard);
	tcase_add_test(tc, test_writen_nbchar);
	tcase_add_test(tc, test_writen_nbchar_nolog);
	tcase_add_test(tc, test_log_preamble);
	tcase_add_test(tc, test_nolog_preamble);
	tcase_add_test(tc, test_log_afterword);
	tcase_add_test(tc, test_nolog_afterword);

	suite_add_tcase(s, tc);

	return s;
}
