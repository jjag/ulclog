/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdarg.h>

#include <check.h>

#include "predictable_time.h"

static void setup(void);
static void teardown(void);

struct Suite *get_suite(void);

#include "ulclog/ulclog_logger.h"

struct ulclog_test_logger {
	struct ulclog_logger ulclog_logger;
	int priority;
	char message[LOG_BUFFERS_SIZE];
	ssize_t dolog_return_value;
};

static int ulclog_test_vctor(
	struct ulclog_logger *, va_list);
static void ulclog_test_dtor(
	struct ulclog_logger *);
static ssize_t ulclog_test_dolog(
	struct ulclog_logger *,
	int,
	const char *,
	ssize_t);

static struct LoggerClass TestLogger = {
	.size = sizeof(struct ulclog_test_logger),
	.vctor = ulclog_test_vctor,
	.dtor  = ulclog_test_dtor,
	.dolog = ulclog_test_dolog,
};

int ulclog_test_vctor(
	struct ulclog_logger *ulclog_logger,
	va_list pa __attribute__((unused)))
{
	struct ulclog_test_logger *ulclog_test_logger =
		(struct ulclog_test_logger *)ulclog_logger;
	ulclog_test_logger->priority = -1;
	ulclog_test_logger->message[0] = '\0';
	ulclog_test_logger->dolog_return_value = 0;

	return 0;
}

void ulclog_test_dtor(
	struct ulclog_logger *ulclog_logger __attribute__((unused)))
{
}

static ssize_t ulclog_test_dolog(
	struct ulclog_logger *ulclog_logger,
	int priority,
	const char *message,
	ssize_t nbchar __attribute__((unused)))
{
	struct ulclog_test_logger *ulclog_test_logger =
		(struct ulclog_test_logger *)ulclog_logger;
	ulclog_test_logger->priority = priority;
	strncpy(ulclog_test_logger->message, message,
		sizeof(ulclog_test_logger->message) - 1);

	return ulclog_test_logger->dolog_return_value;
}

#define NTEST_LOGGERS		3
static struct ulclog_logger *test_loggers[NTEST_LOGGERS];
void setup(void)
{
	for (int i = 0 ; i < NTEST_LOGGERS ; i++)
		test_loggers[i] = ulclog_logger_new(&TestLogger);
}

void teardown(void)
{
	for (int i = 0 ; i < NTEST_LOGGERS ; i++)
		ulclog_logger_delete(test_loggers[i]);
}

#include "ulclog/ulclog_logger_dispatch.h"

START_TEST(test_dispatch_init_clean)
{
	struct ulclog_dispatch_logger logger;

	ulclog_dispatch_init(&logger, 3,
			    test_loggers[0],
			    test_loggers[1],
			    test_loggers[2]);

	ck_assert_ptr_eq(ulclog_dispatch_loggger_get_logger(&logger, 0),
			 test_loggers[0]);
	ck_assert_ptr_eq(ulclog_dispatch_loggger_get_logger(&logger, 1),
			 test_loggers[1]);
	ck_assert_ptr_eq(ulclog_dispatch_loggger_get_logger(&logger, 2),
			 test_loggers[2]);
	ck_assert_ptr_eq(ulclog_dispatch_loggger_get_logger(&logger, 3),
			 NULL);

	ulclog_dispatch_clean(&logger);
}
END_TEST

START_TEST(test_dispatch_new_delete)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&DispatchLogger,
				  3,
				  test_loggers[0],
				  test_loggers[1],
				  test_loggers[2]);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_ptr_eq(ulclog_dispatch_loggger_get_logger(
				 (struct ulclog_dispatch_logger *)logger, 0),
			 test_loggers[0]);
	ck_assert_ptr_eq(ulclog_dispatch_loggger_get_logger(
				 (struct ulclog_dispatch_logger *)logger, 1),
			 test_loggers[1]);
	ck_assert_ptr_eq(ulclog_dispatch_loggger_get_logger(
				 (struct ulclog_dispatch_logger *)logger, 2),
			 test_loggers[2]);
	ck_assert_ptr_eq(ulclog_dispatch_loggger_get_logger(
				 (struct ulclog_dispatch_logger *)logger, 3),
			 NULL);

	ulclog_logger_delete(logger);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#define LOG_METHOD		ulclog_logger_log
#define LOG_CONTEXT(level)	LOG_PARAMS(logger, level)
#define LOG_MODULE		"test_logger"
#define PRE_FORMAT		"Tadaaam!"
#define POST_FORMAT		""

#define LOG(level, format, ...)				\
	LOG_METHOD(LOG_CONTEXT(level),			\
		   PRE_FORMAT format POST_FORMAT,	\
		   ##__VA_ARGS__)

#include "ulclog/ulclog.h"

START_TEST(test_dispatch_log)
{
	struct ulclog_logger *logger;
	ssize_t nbchar;

	logger = ulclog_logger_new(&DispatchLogger,
				  3,
				  test_loggers[0],
				  test_loggers[1],
				  test_loggers[2]);

	((struct ulclog_test_logger *)test_loggers[0])->dolog_return_value = 1;
	((struct ulclog_test_logger *)test_loggers[1])->dolog_return_value = 10;
	((struct ulclog_test_logger *)test_loggers[2])->dolog_return_value =
	  100;

	nbchar = LINFO("INFO");

	ck_assert_int_eq(nbchar, 111);
	ck_assert_str_eq(
		((struct ulclog_test_logger *)test_loggers[0])->message,
		"Tadaaam!INFO");
	ck_assert_str_eq(
		((struct ulclog_test_logger *)test_loggers[1])->message,
		"Tadaaam!INFO");
	ck_assert_str_eq(
		((struct ulclog_test_logger *)test_loggers[2])->message,
		"Tadaaam!INFO");
	ck_assert_int_eq(
		((struct ulclog_test_logger *)test_loggers[0])->priority,
		LOG_INFO);
	ck_assert_int_eq(
		((struct ulclog_test_logger *)test_loggers[1])->priority,
		LOG_INFO);
	ck_assert_int_eq(
		((struct ulclog_test_logger *)test_loggers[2])->priority,
		LOG_INFO);

	ulclog_logger_delete(logger);
}
END_TEST

START_TEST(test_dispatch_log_with_init_clean)
{
	struct ulclog_dispatch_logger dispatch_logger;
	struct ulclog_logger *logger = (struct ulclog_logger *)&dispatch_logger;
	ssize_t nbchar;

	ulclog_dispatch_init(&dispatch_logger,
			    3,
			    test_loggers[0],
			    test_loggers[1],
			    test_loggers[2]);

	((struct ulclog_test_logger *)test_loggers[0])->dolog_return_value = 3;
	((struct ulclog_test_logger *)test_loggers[1])->dolog_return_value = 20;
	((struct ulclog_test_logger *)test_loggers[2])->dolog_return_value =
	  100;

	nbchar = LINFO("INFO");

	ck_assert_int_eq(nbchar, 123);
	ck_assert_str_eq(
		((struct ulclog_test_logger *)test_loggers[0])->message,
		"Tadaaam!INFO");
	ck_assert_str_eq(
		((struct ulclog_test_logger *)test_loggers[1])->message,
		"Tadaaam!INFO");
	ck_assert_str_eq(
		((struct ulclog_test_logger *)test_loggers[2])->message,
		"Tadaaam!INFO");
	ck_assert_int_eq(
		((struct ulclog_test_logger *)test_loggers[0])->priority,
		LOG_INFO);
	ck_assert_int_eq(
		((struct ulclog_test_logger *)test_loggers[1])->priority,
		LOG_INFO);
	ck_assert_int_eq(
		((struct ulclog_test_logger *)test_loggers[2])->priority,
		LOG_INFO);

	ulclog_dispatch_clean(&dispatch_logger);
}
END_TEST

struct Suite *get_suite(void)
{
	struct Suite *s;
	struct TCase *tc;

	s = suite_create("Dispatch back-end");

	/* Core test case */
	tc = tcase_create("Dispatch back-end");

	tcase_add_checked_fixture(tc, setup, teardown);

	tcase_add_test(tc, test_dispatch_init_clean);
	tcase_add_test(tc, test_dispatch_new_delete);
	tcase_add_test(tc, test_dispatch_log);
	tcase_add_test(tc, test_dispatch_log_with_init_clean);

	suite_add_tcase(s, tc);

	return s;
}
