/* Copyright 2016 Jérôme Arbez-Gindre */

/* This file is part of UlClog. */

/* UlClog is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version. */

/* UlClog is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public
 * License along with UlClog.  If not, see
 * <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

#include <check.h>

#include "predictable_time.h"

static void setup(void);

struct Suite *get_suite(void);

/* openlog stub */
static const char *openlog_stub_ident;
static int openlog_stub_option;
static int openlog_stub_facility;
void openlog(
	const char *ident,
	int option,
	int facility)
{
	openlog_stub_ident = ident;
	openlog_stub_option = option;
	openlog_stub_facility = facility;
}

/* closelog stub */
bool closelog_stub_called;
void closelog(void)
{
	closelog_stub_called = true;
}

/* setlogmask stub */
int setlogmask_stub_mask;
int setlogmask(
	int mask)
{
	int previous_mask = setlogmask_stub_mask;

	setlogmask_stub_mask = mask;
	return previous_mask;
}

/* syslog stub */
static int syslog_stub_priority;
static char syslog_stub_message[LOG_BUFFERS_SIZE];
void syslog(
	int priority,
	const char *format,
	...)
{
	va_list pa;

	syslog_stub_priority = priority;

	va_start(pa, format);
	vsnprintf(syslog_stub_message, sizeof(syslog_stub_message), format, pa);
	va_end(pa);
}

void setup(void)
{
	reset_gettimeofday_ref();

	openlog_stub_ident = "";
	openlog_stub_option = -1;
	openlog_stub_facility = -1;

	closelog_stub_called = false;

	setlogmask_stub_mask = 0;

	syslog_stub_priority = INT32_MAX;
	syslog_stub_message[0] = '\0';
}

#include "ulclog/ulclog_logger_syslog.h"

START_TEST(test_syslog_init_clean)
{
	struct ulclog_logger ulclog_logger;
	int init_res;

	init_res = ulclog_syslog_init(&ulclog_logger, "pouet",
				     LOG_PERROR | LOG_PID,
				     LOG_USER);

	ck_assert_int_eq(init_res, 0);
	ck_assert_str_eq(openlog_stub_ident, "pouet");
	ck_assert_int_eq(openlog_stub_option, LOG_PERROR | LOG_PID);
	ck_assert_int_eq(openlog_stub_facility, LOG_USER);

	ulclog_syslog_clean(&ulclog_logger);

	ck_assert_int_eq(closelog_stub_called, true);
}
END_TEST

START_TEST(test_syslog_new_delete)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&SyslogLogger,
				  "pouet", LOG_PERROR | LOG_PID,
				  LOG_USER);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_str_eq(openlog_stub_ident, "pouet");
	ck_assert_int_eq(openlog_stub_option, LOG_PERROR | LOG_PID);
	ck_assert_int_eq(openlog_stub_facility, LOG_USER);

	ulclog_logger_delete(logger);

	ck_assert_int_eq(closelog_stub_called, true);
}
END_TEST

START_TEST(test_syslog_new_delete_pouet)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&SyslogLogger,
				  "pouet", LOG_PERROR | LOG_PID,
				  LOG_USER);

	ck_assert_ptr_ne(logger, NULL);
	ck_assert_str_eq(openlog_stub_ident, "pouet");
	ck_assert_int_eq(openlog_stub_option, LOG_PERROR | LOG_PID);
	ck_assert_int_eq(openlog_stub_facility, LOG_USER);

	ulclog_logger_delete(logger);

	ck_assert_int_eq(closelog_stub_called, true);
}
END_TEST


START_TEST(test_syslog_setlogmask)
{
	struct ulclog_logger *logger;

	logger = ulclog_logger_new(&SyslogLogger,
				  "pouet", LOG_PERROR | LOG_PID,
				  LOG_USER);

	setlogmask_stub_mask = LOG_MASK(LOG_WARNING);

	int previous_mask = ulclog_syslog_setlogmask(logger,
						    LOG_UPTO(LOG_NOTICE));

	ck_assert_int_eq(previous_mask, LOG_MASK(LOG_WARNING));
	ck_assert_int_eq(setlogmask_stub_mask, LOG_UPTO(LOG_NOTICE));

	ulclog_logger_delete(logger);
}
END_TEST

#include "ulclog/ulclog_undef_all.h"
#define LOG_METHOD		ulclog_logger_log
#define LOG_CONTEXT(level)	LOG_PARAMS(logger, level)
#define LOG_MODULE		"test_logger"
#include "ulclog/ulclog.h"
START_TEST(test_syslog_log)
{
	struct ulclog_logger *logger;
	ssize_t nbchar;

	logger = ulclog_logger_new(&SyslogLogger,
				  NULL,
				  LOG_NDELAY | LOG_PERROR | LOG_PID,
				  LOG_USER);

	nbchar = LINFO("INFO");

	ck_assert_int_eq(syslog_stub_priority, LOG_INFO);
	ck_assert_str_eq(syslog_stub_message,
			 "1000.999999:info   :6:test_logger:test_syslog_log_fn:INFO\n");
	ck_assert_int_eq(nbchar, 58);

	ulclog_logger_delete(logger);
}
END_TEST

START_TEST(test_syslog_log_with_init_clean)
{
	struct ulclog_logger ulclog_logger;
	struct ulclog_logger *logger = &ulclog_logger;
	ssize_t nbchar;

	ulclog_syslog_init(&ulclog_logger,
			  NULL,
			  LOG_NDELAY | LOG_PERROR | LOG_PID,
			  LOG_USER);

	nbchar = LINFO("INFO");

	ck_assert_int_eq(syslog_stub_priority, LOG_INFO);
	ck_assert_str_eq(syslog_stub_message,
			 "1000.999999:info   :6:test_logger:test_syslog_log_with_init_clean_fn:INFO\n");
	ck_assert_int_eq(nbchar, 74);

	ulclog_syslog_clean(&ulclog_logger);
}
END_TEST

struct Suite *get_suite(void)
{
	struct Suite *s;
	struct TCase *tc;

	s = suite_create("Syslog back-end");

	/* Core test case */
	tc = tcase_create("Syslog back-end");

	tcase_add_checked_fixture(tc, setup, NULL);

	tcase_add_test(tc, test_syslog_init_clean);
	tcase_add_test(tc, test_syslog_new_delete);
	tcase_add_test(tc, test_syslog_new_delete_pouet);
	tcase_add_test(tc, test_syslog_setlogmask);
	tcase_add_test(tc, test_syslog_log);
	tcase_add_test(tc, test_syslog_log_with_init_clean);

	suite_add_tcase(s, tc);

	return s;
}

